<?php
/**
 * TCCS Intercollegiate template for displaying the header
 *
 * @package WordPress
 * @subpackage TCCS Intercollegiate
 * @since TCCS Intercollegiate 2.0
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie ie-no-support" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="ie ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <title><?php wp_title('&raquo;', true, 'right');	?></title>
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <a class="skip-link screen-reader-text skip-content" href="#content"
    tabindex="1"><?php esc_html_e( 'Skip to content', 'tccs-intercollegiate' ); ?></a>

  <a class="skip-link screen-reader-text skip-search" href="#search"
    tabindex="2"><?php esc_html_e( 'Skip to search', 'tccs-intercollegiate' ); ?></a>

  <?php
	// Check if an archive or post page which includes a latest posts sidebar
	if (is_archive() || is_single() ) {
?>
  <a class="skip-link screen-reader-text skip-sidebar" href="#sidebar"
    tabindex="3"><?php esc_html_e( 'Skip to sidebar', 'tccs-intercollegiate' ); ?></a>
  <?php
	}
?>

  <a class="skip-link screen-reader-text skip-footer" href="#footer"
    tabindex="4"><?php esc_html_e( 'Skip to footer', 'tccs-intercollegiate' ); ?></a>

  <div style="height: 0; width: 0; position: absolute; top: 0; visibility: hidden; display: none;">
    <?php
			include('template-parts/common/svg.svg');
		?>
  </div>

  <!-- Modal Navigation -->
  <?php	get_template_part('template-parts/common/header', 'common'); ?>

  <div id="content" class="main-wrapper">
