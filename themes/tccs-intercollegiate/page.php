<?php

get_header();

global $post;

$childpages = '';

if ( $post->ID > 0 ):

	if ( $post->post_parent ):
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
	else:
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
	endif;

endif;

$classLeft = 'col-12 text-md-left';

if ( $childpages ) {
	$classLeft = 'col-lg-8 text-md-left';
}
?>

<div class="container post">
	<?php
	get_template_part( 'template-parts/common/title', 'common' );
	?>
    <div class="row">

        <div class="<?php echo $classLeft; ?>">

            <div class="main">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content();
						?>
					<?php endwhile; ?>
				<?php endif; ?>
            </div>

        </div>

		<?php if ( $childpages ) : ?>
	    <div class="col-lg-4 d-none d-lg-flex">

		<?php get_template_part( 'template-parts/common/menu-page' ); ?>

	    </div>

		<?php endif; ?>

    </div>
</div>

<?php get_footer(); ?>
