<?php
/**
 * Top Navigation - slug 'top-nav'
 * Since Version 1.0.8
 * 
 * Menu - Exclude Pages
 * Set in Admin Dashboard Themes Menu (Not Appearance > Themes Menu)
 */

if ( has_nav_menu( 'top-nav') ) {

wp_nav_menu( array(
	'theme_location'	=> 'top-nav',
	'container'		=> '',
	'items_wrap'		=> '<ol class="navbar-nav mr-auto" id="desktop-menu">%3$s</ol>',
	'before'		=> '<li class="page_item menu-item page_item_has_children dropdown  nav-item">',
	'after'			=> '</li>',
	'walker'		=> new WP_Bootstrap_Navwalker(),

) ); 

} else {
// Verify that ACF is active and fields are set
	if ( function_exists( 'get_field' ) ) {

		if (isset( $exclude_pages ) ) {

			wp_list_pages( array(
				'title_li'    => '',
				'sort_column' => 'menu_order',
				'depth'       => 2,
				'walker'      => new COL_Main_Menu_Custom_Walker(),
				'exclude'     => $exclude_pages, // variable set in functions.php
				'id'          => 'desktop',
				'container'		=> 'ol',
				'container_class'	=> 'navbar-nav mr-auto',
				'container_id'		=> 'desktop-menu',
			) );


		} else {

		wp_list_pages( array(
			'title_li'    => '',
			'sort_column' => 'menu_order',
			'depth'       => 2,
			'walker'      => new COL_Main_Menu_Custom_Walker(),
			'id'          => 'desktop',
			'container'		=> 'ol',
			'container_class'	=> 'navbar-nav mr-auto',
			'container_id'		=> 'desktop-menu',
		) );

		}

	} else {

		// If Advanced Custom Fields Plugin is not active 	
		wp_list_pages( array(
			'title_li'    => '',
			'sort_column' => 'menu_order',
			'depth'       => 2,
			'walker'      => new COL_Main_Menu_Custom_Walker(),
			'exclude'     => '',
			'id'          => 'desktop'
		) );
	}

}
