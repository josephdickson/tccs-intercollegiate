<div id="menu-modal" aria-hidden='false' class="dialog" role="dialog" aria-labelledby="Site Navigation" aria-describedby="Links to all site pages, social media links and site content search form">


    <nav role='navigation'>

        <!-- Body -->
        <div class="text-center">

            <div class="block">

                <!-- Menu -->
                <div class="menu" tabindex='1'>
                    <?php
                        wp_nav_menu( array(
                            'menu' => 'Menu'
                        ) );
                    ?>
                </div>

                <!-- Social Media links -->
                <div class="social fullscreen d-md-none">
                    <?php
                        get_template_part('template-parts/common/social', 'common');
                    ?>
                </div>

            </div>

        </div>


</div>
