<?php
/* *
 * Displays additional information and an edit post link
 * Uses WP dashicons
 * https://developer.wordpress.org/resource/dashicons
 * -- Joseph Dickson
 */
?>

<div class="post-meta-box mb-3">

    <small class="text-muted"><span class="dashicons-before dashicons-category"> <?php the_category(', ') ?></span></small>

    <small class="text-muted"><span class="dashicons-before dashicons-clock"> <span class="time"><?php the_time('l, F j, Y') ?></span></span></small>
</div>

