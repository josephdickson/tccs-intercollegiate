<?php
/* *
 * WP_Query post type and number of posts to display.
 * https://codex.wordpress.org/Class_Reference/WP_Query
 */

    $query  = new WP_Query(
        array (
            'post_type'      => 'post',
            'posts_per_page' => 5,
	        'status' => 'publish'
        )
    );

    // The Loop
    if ( $query->have_posts() ) {
        echo '<h3>Latest Posts</h3>';

	echo '<ul class="menu">';
        while ( $query->have_posts() ) {
            $query->the_post();
            echo '<a class="list-group-item list-group-item-action" href="' . get_the_permalink() . '">';
            echo the_title();
            echo '</a>';
        }
	echo '</ul>';
        /* Restore original Post Data */
        wp_reset_postdata();
    } else {
        // no posts found
    }
