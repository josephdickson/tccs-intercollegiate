<?php
/**
 * Page Menu
 * Since Version 1.0.8
 *
 * If "Page Menu" is assigned display that menu on pages. Otherwise list automatically assigned child pages.
 */

if ( has_nav_menu( 'page-menu' ) ) {

wp_nav_menu( array(
	'theme_location'	=> 'page-menu',
	'container'		=> 'nav',
	'container_class'	=> 'page-nav',
	'walker'		=> new Page_Menu(),
) );  

} else {
	echo wpb_list_child_pages();
}

/**
 * Custom walker class
 * https://developer.wordpress.org/reference/functions/wp_nav_menu/#comment-207
 *
 * Add class of "children" to menus items with children beyond level 2
 */
class Page_Menu extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// Depth-dependent classes.
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$display_depth = ( $depth + 1 ); // because it counts the first submenu as 0
		$classes = array(
		    'sub-menu',
		    ( $display_depth >=2 ? 'children' : '' ), // Adds class .children to level 2 and greater
		    'menu-depth-' . $display_depth
		);
		$class_names = implode( ' ', $classes );

		// Build HTML for output.
		$output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
	}
}
