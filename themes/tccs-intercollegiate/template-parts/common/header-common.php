<?php
/**
 * Check if the Advanced Custom Fields plugin is active
 * Displays variables assigned in Admin Dashboard
 */
if ( function_exists( 'get_field' ) ) {
	// Display defaults from Header Menu in Admin Dashboard 
	if ( have_rows( 'col_logo_settings', 'header_options' ) ) {
		while ( have_rows( 'col_logo_settings', 'header_options' ) ) {

			the_row();

			$logo_alignment = get_sub_field( 'col_logo_alignment', 'header_options' );

			$logo_align_class = ( $logo_alignment == 'center' ) ? ' text-lg-center' : '';

			$logo_type = get_sub_field( 'col_logo_type', 'header_options' );

			if ( $logo_type == 'text_logo' ) {

				$logo_text = get_sub_field( 'col_logo_text', 'header_options' );

				$logo_sub_text = get_sub_field( 'col_logo_subtitle', 'header_options' );

				$logo_size = get_sub_field( 'col_text_logo_sizes', 'header_options' );

				$logo_class = 'cla-text-' . $logo_size;

				$header_no_padding_class = '';

			} elseif ( $logo_type == 'image_logo' ) {

				$image_logo_size = get_sub_field( 'col_image_logo_size', 'header_options' );

				$header_no_padding_class = ( get_sub_field( 'col_padding_around_header_image', 'header_options' ) ) ? '' : ' col-no-padding';

				if ( $image_logo_size == 'full_banner' ) {

					$logo_image   = get_sub_field( 'col_logo_image_full_banner', 'header_options' );

					$logo_image_x2 = get_sub_field( 'col_logo_image_full_banner', 'header_options' );

				} elseif ( $image_logo_size == 'half_banner' ) {

					$logo_image   = get_sub_field( 'col_logo_image_half_banner', 'header_options' );

					$logo_image_x2 = get_sub_field( 'col_logo_image_half_banner', 'header_options' );

				} elseif ( $image_logo_size == 'quarter_banner' ) {

					$logo_image   = get_sub_field( 'col_logo_image_quarter_banner', 'header_options' );

					$logo_image_x2 = get_sub_field( 'col_logo_image_quarter_banner', 'header_options' );

				}
			}
		}
	}

	// Google Search ID 
	$google_custom_search_id = '';

	if ( have_rows( 'col_google_settings', 'header_options' ) ) {

		while ( have_rows( 'col_google_settings', 'header_options' ) ): the_row();

		$google_custom_search_id = get_sub_field( 'col_google_custom_search_id', 'header_options' );

		endwhile;
	}
}

?>

<div class="cla-navigation">

    <header class="cla-header <?php echo isset ( $image_logo_size ) ? $image_logo_size : ''; ?>">

	<div class="container">

	    <div class="row">

		<div class="col-11 col-lg-12">

		    <div class="site-title<?php echo isset ( $logo_align_class ) ? $logo_align_class : ''; ?> <?php echo isset ( $header_no_padding_class ) ? $header_no_padding_class : '';  ?>">

			<a href="<?php echo get_site_url(); ?>" class="logo-link">

<?php
/**
 * If Image Logo is set display the image
 * Otherwise display Text Logo field
 */
if ( isset ( $logo_type ) ) {

	if ( $logo_type == 'image_logo' ) {

		echo '<img class="img-fluid" srcset="' . $logo_image . ' 1x,' . $logo_image_x2 . ' 2x" alt="Logo"/>';

	} else {

		echo $logo_text;

		echo ( $logo_type == 'text_logo' );

		echo '<span class="cla-logo-text' . $logo_class . '">' . $logo_text . '</span>';

		echo '<span class="cla-logo-subtext">' . $logo_sub_text . '</span>';

	}

}

/**
 *
 * Advanced Custom Fields Plugin not active 
 *
 * Use Site settings to display Title and Tagline
 * from Admin Dashboard General Site Settings 
 *
 */
if ( ! function_exists( 'get_field' ) ) {

	echo '<span class="site-title">' . get_bloginfo( 'name' ) . '</span>';

	echo ( bloginfo( 'description' ) ) ? '<br /><span class="site-description">' . get_bloginfo( 'description') . '</span>' : '' ;

}
?>
 
                        </a>
                    </div>
                </div>
                <div class="col-1 d-md-flex d-lg-none">
                    <div class="cla-hamburger-icon-wrapper d-md-block d-lg-none">
                        <span class="hamburger-icon"></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="cla-navbar d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg navbar-light">
				<?php get_template_part( 'template-parts/common/header-menu' ); ?>
                        <div class="cla-social">
<?php
if ( function_exists( 'get_field' ) ) {
	get_template_part( 'template-parts/common/social', 'common' );

	if ( ! empty( trim( $google_custom_search_id ) ) && strpos(get_site_url(), 'thehive') === false ):
	
?>

                                <!-- Search -->
                                <a href="<?php echo get_site_url(); ?>/search/" class="button search"
                                   aria-label="Search"
                                   title="Enter the term you want to search for" id="search">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
<?php

endif;

}
?>

                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="cla-navbar-mobile hide d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar">
                        <ol id="mobile-menu">
<?php
if ( function_exists( 'get_field' ) ) {

	if ( $exclude_pages ) {

	wp_list_pages( array(
		'title_li'    => '',
		'sort_column' => 'menu_order',
		'depth'       => 2,
		'walker'      => new COL_Main_Menu_Custom_Walker(),
		'exclude'     => $exclude_pages, // variable set in functions.php
		'id'          => 'mobile'
	) );

	} else {

		wp_list_pages( array(
			'title_li'    => '',
			'sort_column' => 'menu_order',
			'depth'       => 2,
			'walker'      => new COL_Main_Menu_Custom_Walker(),
			'exclude'     => '',
			'id'          => 'mobile-menu'
		) );

	}

} else {
	// If Advanced Custom Fields Plugin is not active 	
	wp_list_pages( array(
		'title_li'    => '',
		'sort_column' => 'menu_order',
		'depth'       => 2,
		'walker'      => new COL_Main_Menu_Custom_Walker(),
		'exclude'     => '',
		'id'          => 'mobile-menu'
	) );

}

?>
                        </ol>
                    </nav>
                    <div class="cla-social">
<?php

get_template_part( 'template-parts/common/social', 'common' );

/**
 * Google Search ID
 * Set in Admin Dashboard Header Menu
 */
if ( function_exists( 'get_field' ) ) {

	if ( ! empty( trim( $google_custom_search_id ) ) && strpos(get_site_url(), 'thehive') === false ) {

		echo '<a href="' . get_site_url() . '/search/" class="button search" aria-label="Search" title="Search" id="search">';

		echo '<i class="fa fa-search" aria-hidden="true"></i>';

		echo '</a>';

	}
}

?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>'
