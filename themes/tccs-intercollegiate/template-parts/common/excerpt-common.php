<?php
/* *
 * Displays Post excerpt for archive pages
 * or homepage if set to show posts.
 */
global $post;

$args = array(
	'post_type'      => 'post',
	'posts_per_page' => 6,
	'status'         => 'publish',
	'orderby'        => 'date',
	'order'          => 'desc',
	'paged'          => ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1
);

if ( get_query_var( 'cat' ) ) {
	$args['cat'] = get_query_var( 'cat' );
}

$posts = new WP_Query( $args );

/* check for posts */
if ( $posts->have_posts() ):
	/* Start the Loop */
	while ( $posts->have_posts() ) :
		$posts->the_post();

		?>
        <div <?php post_class( 'card mb-3' ); ?>>
            <div class="row no-gutters">
				<?php
				if ( has_post_thumbnail() ) {
					?>
                    <div class="col-12">
					<a href="<?php echo get_the_permalink(); ?>" aria-label="<?php echo get_the_title().' - '.get_the_time('l, F j, Y'); ?>">
						<?php the_post_thumbnail( 'carousel', [ 'class' => 'card-img img-fluid', 'alt' => get_the_title().' - '.get_the_time('l, F j, Y') ] ); ?>
					</a>
                    </div>
					<?php
				}
				?>
                <div class="col-12">
                    <div class="card-body">
                        <h5 class="card-title"><a
                                    href="<?php echo esc_url( get_permalink() ); ?>"
                                    aria-label="<?php echo get_the_title(); ?>"
                                    tabindex="-1"><?php echo get_the_title(); ?></a>
                        </h5>
                        <p class="card-text">
							<?php get_template_part( 'template-parts/common/post', 'meta' ); ?>
                        </p>
                        <p class="card-text">
							<?php
							$excerpt = trim( get_the_excerpt() );

							if ( ! empty( $excerpt ) ) {
								echo $excerpt;
							} else {
								echo wp_trim_words( wp_strip_all_tags( get_the_content() ), 80 );
							}
							?>
                        </p>
                        <p class="card-text">
                            <a href="<?php echo esc_url( get_permalink() ); ?>"
                               aria-label="Read More - <?php echo get_the_title(); ?>"
                               class="btn btn-outline-primary float-right mb-3">Read More</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
	<?php

	endwhile;

	wp_reset_postdata();
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
