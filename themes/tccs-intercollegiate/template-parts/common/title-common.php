<?php
global $post;
?>
<div class="row">
    <div class="col-sm-12">
		<?php get_template_part( 'template-parts/common/breadcrumbs', 'common' ); ?>
		<?php the_title( '<h1 class="entry-title pb-0">', '</h1>' ); ?>
    </div>
</div>

<?php
$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );

if ( ! empty( $featured_image_url ) ) {
	?>
    <div class="row">
        <div class="col-sm-12">
            <figure class="wp-post-image">
				<?php
				// display custom image size tccs-post-thumbnail declared in functions.php
				the_post_thumbnail( 'carousel', [ 'class' => 'img-fluid' ] );

				// check for post thumbnail caption
				if ( has_excerpt( get_post_thumbnail_id( 'carousel' ) ) ) {

					// display caption if it exists
					echo '<figcaption>' . get_post( get_post_thumbnail_id() )->post_excerpt . '</figcaption>';
				}
				?>
            </figure>
        </div>
    </div>

<?php } ?>
