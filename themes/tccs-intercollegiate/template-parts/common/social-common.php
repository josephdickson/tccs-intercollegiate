<?php
/**
 * Check that the Advanced Custom Fields plugin is active
 * and fields are set for available social media accounts
 */
if ( function_exists( 'get_field' ) ) {
	if ( have_rows( 'col_social_icon_settings', 'header_options' ) ) {
		while ( have_rows( 'col_social_icon_settings', 'header_options' ) ) {
			the_row();
			$twitter   = get_sub_field( 'col_social_twitter_url' );
			$instagram = get_sub_field( 'col_social_instagram_url' );
			$facebook  = get_sub_field( 'col_social_facebook_url' );
			$contact   = get_sub_field( 'col_social_email_url' );
		}
	}
}

if ( isset ( $facebook ) ) {
	echo '<a href="' . $facebook . '" class="button facebook" aria-label="Facebook" target="_blank">';
	echo '<span class="dashicons dashicons-facebook"></span>';
    	echo '</a>';
}

if ( isset ( $instagram ) ) {
	echo '<a href="' . $instagram . '" class="button instagram" aria-label="Instagram" target="_blank">';
	echo '<span class="dashicons dashicons-instagram"></span>';
	echo '</a>';
}

if ( isset ( $twitter ) ) {
	echo '<a href="' . $twitter . '" class="button twitter" aria-label="Twitter" target="_blank">';
	echo '<span class="dashicons dashicons-twitter"></span>';
	echo '</a>';
}

if ( isset ( $contact ) ) {
	echo '<a href="mailto:' . $contact . '" class="button envelop" aria-label="Email">';
        echo '<span class="dashicons dashicons-email"></span>';
	echo '</a>';
}
