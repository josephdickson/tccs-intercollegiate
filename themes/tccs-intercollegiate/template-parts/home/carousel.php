<?php
// The Query
$carousel_query = new WP_Query( array( 'category_name' => 'carousel' ) );

if ( $carousel_query->have_posts() ) {

	while ( $carousel_query->have_posts() ) {

		$carousel_query->the_post();

			echo '<article>';

				echo '<a href="' . get_permalink() . '">';
		      
					if (has_post_thumbnail() ) {

						// get thumbnail id for use later
						$thumbnail_id = get_post_thumbnail_id();

						echo '<figure>';

							the_post_thumbnail( 'carousel' );

						echo '</figure>';

					}
		      
				echo '</a>';

			echo '<article>';

	}

} else {

	echo wpautop( 'No posts available' );

}
