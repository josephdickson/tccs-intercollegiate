<?php
$bgColor = (!empty(get_sub_field("col_alert_message_color", "home_options"))) ? get_sub_field("col_alert_message_color", "home_options"): "#ffffff";
$iconColor = (!empty(get_sub_field("col_alert_message_icon_color", "home_options"))) ? get_sub_field("col_alert_message_icon_color", "home_options"): "#000000";
$textColor = (!empty(get_sub_field("col_alert_message_text_color", "home_options"))) ? get_sub_field("col_alert_message_text_color", "home_options"): "#000000";
$boxIcon = (!empty(get_sub_field("col_alert_message_icon", "home_options"))) ? '<span class="'.get_sub_field("col_alert_message_icon", "home_options").'" aria-hidden="true"></span>': '<span class="fas fa-exclamation-circle" aria-hidden="true"></span>';
$boxText = (!empty(get_sub_field("col_alert_message_text", "home_options"))) ? get_sub_field("col_alert_message_text", "home_options"): "";
if(!empty(trim($boxText))):
?>
<style type="text/css">
#alert-box {
    padding: 25px;
    background: <?php echo $bgColor; ?>;
    margin: 25px 0;
    font-size: 18px;
    font-weight: bold;
    line-height: 28px;
}
#alert-box span {
    display: inline;
    margin-right: 10px;
    color: <?php echo $iconColor; ?>;
}
#alert-box span ~ p {
    display: inline;
    margin: 0;
    color: <?php echo $textColor; ?>;
}
</style>
<div id="alert-box">
    <?php echo $boxIcon; ?>
    <?php echo $boxText; ?>
</div>
<?php
endif;