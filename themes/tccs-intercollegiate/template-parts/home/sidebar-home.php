<div class="col-12 col-lg-4">
	<?php
	global $col_sidebar_order;
	if ( $col_sidebar_order == 'widget_text' ):
		?>
		<?php if ( is_active_sidebar( 'homepage-side-bar' ) ) : ?>
		<?php dynamic_sidebar( 'homepage-side-bar' ); ?>
	<?php endif;
	endif;
	?>
	<?php
	if ( have_rows( 'col_sidebar_with_text_column', 'home_options' ) ):
		while ( have_rows( 'col_sidebar_with_text_column', 'home_options' ) ): the_row();
			$textSidebarTitle   = get_sub_field( 'col_sidebar_with_text_column_title', 'home_options' );
			$textSidebarContent = get_sub_field( 'col_sidebar_with_text_column_content', 'home_options' );
		endwhile;
		if ( ! empty( trim( $textSidebarContent ) ) ):
			?>
            <div class="widget-content">
                <h2 class="widget-title"><?php echo $textSidebarTitle; ?></h2>
				<?php echo $textSidebarContent; ?>
            </div>
		<?php
		endif;
	endif;
	if ( $col_sidebar_order == 'text_widget' ):
		?>
		<?php if ( is_active_sidebar( 'homepage-side-bar' ) ) : ?>
		<?php dynamic_sidebar( 'homepage-side-bar' ); ?>
	<?php endif;
	endif;
	?>
</div>

