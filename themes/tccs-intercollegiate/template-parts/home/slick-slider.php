<?php
global $sildesReadMoreText;
if (have_rows('col_carousel_slides', 'home_options')) :
?>
    <div class="cla-slider">
        <div id="carouselOne" class="carousel">
            <ul class="slider-wrapper">
                <?php
                while (have_rows('col_carousel_slides', 'home_options')) : the_row();
                    $slideImgUrl = get_sub_field('col_carousel_slide_image', 'home_options');
                    $slideText = get_sub_field('col_carousel_slide_text', 'home_options');
                    $slideLink = get_sub_field('col_carousel_slide_link', 'home_options');
                    $slideReadMore = get_sub_field('col_slides_read_more_text', 'home_options');
                ?>
                    <li class="slide">
                        <?php if (wp_http_validate_url($slideLink)) : ?>
                            <a href="<?php echo $slideLink; ?>" class="cla-post-link" aria-label="<?php echo strip_tags($slideReadMore) . " - " . strip_tags($slideText); ?>" tabindex="-1"></a>
                        <?php endif; ?>
                        <div class="img-wrapper" style="background-image: url('<?php echo esc_url($slideImgUrl); ?>')" aria-hidden="false" aria-label="<?php echo strip_tags($slideReadMore) . " - " . strip_tags($slideText); ?>">
                            <p>
                                <?php echo $slideText; ?>
                                <?php if (wp_http_validate_url($slideLink)) : ?>
                                    <a href="<?php echo $slideLink; ?>" aria-label="<?php echo strip_tags($slideReadMore) . " - " . strip_tags($slideText); ?>">
                                        <?php
                                        echo $slideReadMore;
                                        ?>
                                    </a>
                                <?php endif; ?>
                            </p>
                        </div>
                    </li>
                <?php
                endwhile;
                ?>
            </ul>
        </div>
    </div>
<?php
endif;
?>