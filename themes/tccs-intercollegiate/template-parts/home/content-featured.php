<?php
if ( function_exists( 'get_field' ) ) {

	if ( have_rows( 'col_featured_settings', 'home_options' ) ):
		while ( have_rows( 'col_featured_settings', 'home_options' ) ): the_row();
			$featuredVisibilty = get_sub_field( 'col_featured_visibility', 'home_options' );
			if ( $featuredVisibilty ):
				$featuredArray = array();
				if ( have_rows( 'col_featured_posts', 'home_options' ) ):
					$counter = 0;
					while ( have_rows( 'col_featured_posts', 'home_options' ) ): the_row();
						$featuredArray[ $counter ]['post_title'] = get_sub_field( 'col_featured_post_title', 'home_options' );
						$featuredArray[ $counter ]['post_img']   = get_sub_field( 'col_featured_post_img', 'home_options' );
						$featuredArray[ $counter ]['post_text']  = get_sub_field( 'col_featured_post_text', 'home_options' );
						$featuredArray[ $counter ]['post_link']  = get_sub_field( 'col_featured_post_link', 'home_options' );
						$featuredArray[ $counter ]['post_read_more_text']  = get_sub_field( 'col_featured_post_read_more_text', 'home_options' );
						$counter ++;
					endwhile;
				endif;
				if ( ! empty( $featuredArray ) ):
					?>
			<div class="container featured-container post">
			    <div class="row justify-content-center">
							<?php foreach ( $featuredArray as $post ): ?>

				    <div class="col-sm-12 col-md-4 mb-4 mb-lg-0 d-flex align-items-stretch">
					<div class="card">
					    <div class="card-img-top">
							<?php
							// check if the post has a Post Thumbnail assigned to it.
							if ( ! empty( $post['post_img'] ) ) {

								$imagesUrl = $post['post_img'];

								echo '<a href="' . esc_url( $post['post_link'] ) . '" aria-label="'.($post['post_read_more_text'] ? $post['post_read_more_text']: "").' - '.$post['post_title'].'" tabindex="-1">';

								echo '<img src="' . $imagesUrl . '" style="width: 100%;" alt="'.($post['post_read_more_text'] ? $post['post_read_more_text']: "").' - '.$post['post_title'].'" />';

								echo '</a>';

							}
							?>
					    </div>
					    <div class="card-body">
						<h2 class="card-title"><?php echo $post['post_title']; ?></h2>
						<p><?php echo $post['post_text']; ?></p>
					    </div>
					    <div class="card-footer">
						<a href="<?php echo esc_url( $post['post_link'] ); ?>" class="more-link" aria-label="<?php echo $post['post_read_more_text'] ? $post['post_read_more_text']: "";?> - <?php echo $post['post_title'];?>" tabindex="-1"><?php echo ($post['post_read_more_text']) ? $post['post_read_more_text']: "";?> <?php echo '<span class="dashicons dashicons-arrow-right"></span>'; ?></a>
					    </div>
					</div>
				    </div>

							<?php endforeach; ?>

				<div style="clear:both; "></div>

			    </div>
			</div>

			<div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>
				<?php
				endif;
			endif;
		endwhile;
	endif;

}
