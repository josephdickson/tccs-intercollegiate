<?php

class COL_Main_Menu_Custom_Walker extends Walker_Page {

	public function start_lvl( &$output, $depth = 0, $args = array() ) {

		if ( isset( $args['item_spacing'] ) && 'preserve' === $args['item_spacing'] ) {
			$t = "\t";
			$n = "\n";
		} else {
			$t = '';
			$n = '';
		}
		$indent = str_repeat( $t, $depth );
		$output .= "{$n}{$indent}<ul class='dropdown-menu' role='menu'>{$n}";
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		if ( isset( $args['item_spacing'] ) && 'preserve' === $args['item_spacing'] ) {
			$t = "\t";
			$n = "\n";
		} else {
			$t = '';
			$n = '';
		}
		$indent = str_repeat( $t, $depth );
		$output .= "{$indent}</ul>{$n}";
	}

	public function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
		if ( isset( $args['item_spacing'] ) && 'preserve' === $args['item_spacing'] ) {
			$t = "\t";
			$n = "\n";
		} else {
			$t = '';
			$n = '';
		}
		if ( $depth ) {
			$indent = str_repeat( $t, $depth );
		} else {
			$indent = '';
		}

		$css_class = array( 'page_item', 'menu-item page-item-' . $page->ID );

		if ( isset( $args['pages_with_children'][ $page->ID ] ) ) {
			$css_class[] = 'page_item_has_children';
		}

		if ( ! empty( $current_page ) ) {
			$_current_page = get_post( $current_page );
			if ( $_current_page && in_array( $page->ID, $_current_page->ancestors ) ) {
				$css_class[] = 'current-menu-ancestor';
			}
			if ( $page->ID == $current_page ) {
				$css_class[] = 'current-menu-item';
			} elseif ( $_current_page && $page->ID == $_current_page->post_parent ) {
				$css_class[] = 'current_page_parent';
			}
		} elseif ( $page->ID == get_option( 'page_for_posts' ) ) {
			$css_class[] = 'current_page_parent';
		}

		if ( has_children( $page->ID ) ) {
			$css_class[] = 'dropdown';
		}


		$css_classes = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );
		$css_classes = $css_classes ? ' class="' . esc_attr( $css_classes ) . ' nav-item"' : '';

		if ( '' === $page->post_title ) {
			/* translators: %d: ID of a post */
			$page->post_title = sprintf( __( '#%d (no title)' ), $page->ID );
		}

		$args['link_before'] = empty( $args['link_before'] ) ? '' : $args['link_before'];
		$args['link_after']  = empty( $args['link_after'] ) ? '' : $args['link_after'];

		$atts          = array();
		$atts['title'] = get_the_title( $page->ID );
		$atts['href']  = get_permalink( $page->ID );
		$atts['class'] = ( $page->post_parent > 0 ) ? 'dropdown-item' : 'nav-link';
		if ( has_children( $page->ID ) ) {
			$atts['id'] = 'page-item-dropdown-' . $page->ID;
			if ( $args['id'] ) {
				$atts['id'] = 'page-item-' . $args['id'] . '-dropdown-' . $page->ID;
			}
		}

		if(strtolower($atts['title']) == 'search'){
			$atts['class'] .= ' search';
		}


		$atts = apply_filters( 'page_menu_link_attributes', $atts, $page, $depth, $args, $current_page );

		$dropdownIcon = '';

		if ( has_children( $page->ID ) && $depth < ( $args['depth'] - 1 ) ) {
			$dropdownIcon = '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-chevron-down"></span><span class="sr-only">Toggle '.apply_filters( 'the_title', $page->post_title, $page->ID ).' Dropdown</span></a>';
		}

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value      = esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}


		$liID = 'page-item-' . $page->ID;
		if ( $args['id'] ) {
			$liID = 'page-item-' . $args['id'] . '-' . $page->ID;
		}

		$output .= $indent . sprintf(
				'<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="%s" %s><a%s>%s%s%s</a>%s',
				$liID,
				$css_classes,
				$attributes,
				$args['link_before'],
				/** This filter is documented in wp-includes/post-template.php */
				apply_filters( 'the_title', $page->post_title, $page->ID ),
				$args['link_after'],
				$dropdownIcon
			);

		if ( ! empty( $args['show_date'] ) ) {
			if ( 'modified' == $args['show_date'] ) {
				$time = $page->post_modified;
			} else {
				$time = $page->post_date;
			}

			$date_format = empty( $args['date_format'] ) ? '' : $args['date_format'];
			$output      .= ' ' . mysql2date( $date_format, $time );
		}
	}

	public function end_el( &$output, $page, $depth = 0, $args = array() ) {
		if ( isset( $args['item_spacing'] ) && 'preserve' === $args['item_spacing'] ) {
			$t = "\t";
			$n = "\n";
		} else {
			$t = '';
			$n = '';
		}
		$output .= "</li>{$n}";
	}
}

?>
