<?php 
$custom_walker = new Walker_Sidebar_Menu;

wp_nav_menu( array(
	'theme_location'	=> 'sidebar-menu',
	'container_id'		=> 'sidebar',
	'container_class'	=> 'list-group',
	'fallback_cb' 		=> '', // workaround to show a message to set up a menu
	'items_wrap'     	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'before'		=> '<span class="list-group-item">',
	'after'			=> '</span>',
	'walker'		=> $custom_walker,
) );
