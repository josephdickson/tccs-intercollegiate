var gulp = require('gulp'),
    gutil = require('gulp-util'),
    babel = require('gulp-babel'),
    plumber = require('gulp-plumber')

jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    connect = require('gulp-connect');

input = {
    'sass': 'src/scss/**/*.scss',
    'javascript': ['node_modules/breakpoints-js/dist/breakpoints.js', 'src/javascript/**/*.js']
},

    output = {
        'stylesheets': 'public/stylesheets',
        'javascript': 'public/javascript',
    };

/* run javascript through jshint */
gulp.task('jshint', async function () {
    return gulp.src(input.javascript)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

/* compile scss files */
gulp.task('build-css', async function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(output.stylesheets))
        .pipe(connect.reload());
});

/* concat javascript files, minify if --type production */
gulp.task('build-js', async function () {
    return gulp.src(input.javascript)
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        //only uglify if gulp is ran with '--type production'
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
        .pipe(sourcemaps.write())
        .pipe(plumber())
        .pipe(babel({
            presets: [
                ['@babel/env', {
                    modules: false
                }]
            ]
        }))
        .pipe(gulp.dest(output.javascript))
        .pipe(connect.reload());
});

/* Watch these files for changes and run the task on update */
gulp.task('watch', function () {
    gulp.watch(input.javascript, gulp.series('jshint', 'build-js'));
    gulp.watch(input.sass, gulp.series('build-css'));
});

/* run the watch task when gulp is called without arguments */
gulp.task('default', gulp.parallel('watch'));

/* Modified from Source: https://raw.githubusercontent.com/scotch-io/gulp-start/master/gulpfile.js and http://ilikekillnerds.com/2014/07/how-to-basic-tasks-in-gulp-js/*/
