<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-12">
<?php
/**
 * Check if Advanced Custom Fields plugin is active
 * Display Homepage settings from Admin Dashboard
 */
if ( function_exists( 'get_field' ) ) {
	if ( have_rows( 'col_alert_message_settings', 'home_options' ) ):
		while ( have_rows( 'col_alert_message_settings', 'home_options' ) ): the_row();
			$alertMessageVisilty = get_sub_field( 'col_alert_message_visibility' );

			if ( $alertMessageVisilty ):
				get_template_part( 'template-parts/home/alert', 'message' );
			endif;

		endwhile;
	endif;
	if ( have_rows( 'col_carousel_settings', 'home_options' ) ):
		while ( have_rows( 'col_carousel_settings', 'home_options' ) ): the_row();
			$silderVisilty = get_sub_field( 'col_carousel_visibility' );

			if ( $silderVisilty ):
				get_template_part( 'template-parts/home/slick', 'slider' );
			endif;

		endwhile;
	endif;
	if ( have_rows( 'col_content_settings', 'home_options' ) ):
		while ( have_rows( 'col_content_settings', 'home_options' ) ): the_row();
			$content_column   = get_sub_field( 'col_content_column', 'home_options' );
			$col_content_text = get_sub_field( 'col_content_text', 'home_options' );
			$col_sidebar_order = get_sub_field( 'col_sidebar_order', 'home_options' );

			$colClass = ( $content_column == 'full_width' ) ? 'col-12' : 'col-12 col-lg-8';
?>

                    <div class="row">
                        <div class="<?php echo $colClass; ?>">
							<?php echo $col_content_text; ?>
                        </div>
						<?php
						if ( $content_column == 'with_sidebar' ):
							get_template_part( 'template-parts/home/sidebar', 'home' );
						endif;
						?>
                    </div>
<?php

endwhile;

endif;

} else {
	/**
	 * If Advanced Custom Fields is not set and Homepage
	 * is set to a single page in Admin Dashboard, Reading Settings
	 */
	
	global $post;

	$child_pages = '';

	if ( $post->ID > 0 ) {

		if ( $post->post_parent ) {

			$child_pages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );

		} else {

			$child_pages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
		}
	}


	$class_left = 'col-12 text-center text-md-left';

	if ( $child_pages ) {

		$class_left = 'col-lg-8 text-center text-md-left';
	}

	echo '<div class="container post">';

	get_template_part( 'template-parts/common/title', 'common' );

	echo '<div class="row">';

	echo '<div class="' . $class_left . '">';

	echo '<div class="main">';

	if ( have_posts() ) {

		while ( have_posts() ) {

			the_post();

			the_content();

		}

	}

	echo '</div>';

	echo '</div>';

	if ( $child_pages ) {

		echo '<div class="col-lg-4 d-none d-lg-flex">';

		echo wpb_list_child_pages();

		echo '</div>';

	}


	echo '</div>';

	echo '</div>';

}

?>
		</div>
	</div>
</div>


<div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>

<?php get_template_part( 'template-parts/home/content', 'featured' ); ?>

<?php get_footer(); ?>
