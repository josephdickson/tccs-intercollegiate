<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>Page not found</h1>
            <p>Sorry but the page you are looking does not exist, please try again.</p>
        </div>
    </div>
</div>

<?php get_footer(); ?>
