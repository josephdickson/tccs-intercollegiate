jQuery(document).ready(($) => {
  const prefix = "cla";

  Breakpoints();

  const navigation = () => {
    const $iconMainMenuWrapper = $(
      `.${prefix}-header .${prefix}-hamburger-icon-wrapper`
    );
    const $navMob = $(`.${prefix}-navbar-mobile`);
    const header = $(`.${prefix}-header`);
    const top = 192;

    let didResize;

    $(window).resize(() => {
      didResize = true;
    });

    const smallDevicesSwitcher = () => {
      if (Breakpoints.is("md")) {
        $iconMainMenuWrapper.find(".hamburger-icon").removeClass("animate");
        $navMob.slideUp(0);
      }
    };

    setInterval(() => {
      if (didResize) {
        smallDevicesSwitcher();
        didResize = false;
      }
    }, 250);

    $iconMainMenuWrapper.click(function () {
      $(this).find(".hamburger-icon").toggleClass("animate");

      $navMob.slideToggle(250);
    });

    $(window).scroll(() => {
      if ($(window).scrollTop() >= top) {
        header.addClass("minimize");
      } else {
        header.removeClass("minimize");
      }
    });
  };

  const w = window;
  const d = w.document;

  const addPolyfill = (e) => {
    const type = e.type === "focus" ? "focusin" : "focusout";
    const event = new CustomEvent(type, {
      bubbles: true,
      cancelable: false,
    });
    event.c1Generated = true;
    e.target.dispatchEvent(event);
  };

  const removePolyfill = (e) => {
    if (!e.c1Generated) {
      // focus after focusin, so chrome will the first time trigger tow times focusin
      d.removeEventListener("focus", addPolyfill, true);
      d.removeEventListener("blur", addPolyfill, true);
      d.removeEventListener("focusin", removePolyfill, true);
      d.removeEventListener("focusout", removePolyfill, true);
    }
    setTimeout(() => {
      d.removeEventListener("focusin", removePolyfill, true);
      d.removeEventListener("focusout", removePolyfill, true);
    });
  };

  if (w.onfocusin === undefined) {
    d.addEventListener("focus", addPolyfill, true);
    d.addEventListener("blur", addPolyfill, true);
    d.addEventListener("focusin", removePolyfill, true);
    d.addEventListener("focusout", removePolyfill, true);
  }

  const slider = () => {
    const MyCarousel = function () {
      const setMotionDuration = 5000;

      let carousel;
      let slides;
      let index;
      let slidenav;
      let settings;
      let timer;
      let setFocus;
      let animationSuspended;

      const setSlides = (
        newCurrent,
        setFocusHere,
        transition,
        announceItemHere
      ) => {
        setFocus = typeof setFocusHere !== "undefined" ? setFocusHere : false;
        const announceItem =
          typeof announceItemHere !== "undefined" ? announceItemHere : false;
        const trans = typeof transition !== "undefined" ? transition : "none";
        const newCurr = parseFloat(newCurrent);
        const { length } = slides;

        let newNext = newCurr + 1;
        let newPrev = newCurr - 1;

        if (newNext === length) {
          newNext = 0;
        } else if (newPrev < 0) {
          newPrev = length - 1;
        }

        for (let i = slides.length - 1; i >= 0; i -= 1) {
          slides[i].className = "slide";
        }

        slides[newNext].className = `next slide${
          trans === "next" ? " in-transition" : ""
        }`;

        if (length === 2) {
          if (newCurr === 0) {
            slides[newNext].className = `prevtwo next slide in-transition`;
          } else if (newCurr === 1) {
            slides[newNext].className = `nexttwo next slide in-transition`;
          }
        }

        slides[newNext].setAttribute("aria-hidden", "true");

        if (newNext !== newPrev) {
          slides[newPrev].className = `prev slide${
            trans === "prev" ? " in-transition" : ""
          }`;
          slides[newPrev].setAttribute("aria-hidden", "true");
        }

        slides[newCurr].className = "current slide";
        slides[newCurr].removeAttribute("aria-hidden");

        if (announceItem) {
          carousel.querySelector(".liveregion").textContent = `Item ${
            newCurr + 1
          } of ${slides.length}`;
        }

        if (settings.slidenav) {
          const buttons = carousel.querySelectorAll(
            ".slidenav button[data-slide]"
          );
          for (let j = buttons.length - 1; j >= 0; j -= 1) {
            buttons[j].className = "";
            buttons[j].innerHTML = `<span class="visuallyhidden">Slide</span> ${
              j + 1
            }`;
          }
          buttons[newCurr].className = "current";
          buttons[
            newCurr
          ].innerHTML = `<span class="visuallyhidden">Slide</span> ${
            newCurr + 1
          } <span class="visuallyhidden">(Current Item)</span>`;
        }

        index = newCurr;
      };

      const nextSlide = (announceItem) => {
        const annItem =
          typeof announceItem !== "undefined" ? announceItem : false;

        const { length } = slides;
        let newCurrent = index + 1;

        if (newCurrent === length) {
          newCurrent = 0;
        }

        setSlides(newCurrent, false, "prev", annItem);

        if (settings.animate) {
          timer = setTimeout(nextSlide, setMotionDuration);
        }
      };

      const prevSlide = (announceItem) => {
        const annItem =
          typeof announceItem !== "undefined" ? announceItem : false;
        const { length } = slides;
        let newCurrent = index - 1;

        if (newCurrent < 0) {
          newCurrent = length - 1;
        }

        setSlides(newCurrent, false, "next", annItem);
      };

      const stopAnimation = () => {
        clearTimeout(timer);
        settings.animate = false;
        animationSuspended = false;

        const btn = carousel.querySelector("[data-action]");
        btn.innerHTML =
          '<span class="visuallyhidden">Start Animation </span><i class="fa fa-play" aria-hidden="true"></i>';
        btn.setAttribute("data-action", "start");
      };

      const startAnimation = () => {
        settings.animate = true;
        animationSuspended = false;
        timer = setTimeout(nextSlide, setMotionDuration);

        const btn = carousel.querySelector("[data-action]");
        btn.innerHTML =
          '<span class="visuallyhidden">Stop Animation </span><i class="fa fa-stop" aria-hidden="true"></i>';
        btn.setAttribute("data-action", "stop");
        btn.classList.add("stop");
      };

      const suspendAnimation = () => {
        if (settings.animate) {
          clearTimeout(timer);
          settings.animate = false;
          animationSuspended = true;
        }
      };

      const forEachElement = (elements, fn) => {
        for (let i = 0; i < elements.length; i += 1) fn(elements[i], i);
      };

      const removeClass = (el, className) => {
        const elem = el;

        if (elem.classList) {
          elem.classList.remove(className);
        } else {
          elem.className = elem.className.replace(
            new RegExp(`(^|\\b)${className.split(" ").join("|")}(\\b|$)`, "gi"),
            " "
          );
        }
      };

      const hasClass = (el, className) => {
        if (el.classList) {
          return el.classList.contains(className);
        }
        return new RegExp(`(^| )${className}( |$)`, "gi").test(el.className);
      };

      const init = (set) => {
        settings = set;
        carousel = document.getElementById(settings.id);
        slides = carousel.querySelectorAll(".slide");

        carousel.className = "active carousel";

        const ctrls = document.createElement("ul");

        ctrls.className = "controls full";
        ctrls.innerHTML = `
        <li class="button-prev">
          <button type="button" class="btn-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
        </li>
        <li class="button-prev">
          <button type="button" class="btn-next"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        </li>`;

        ctrls.querySelector(".btn-prev").addEventListener("click", () => {
          prevSlide(true);
        });
        ctrls.querySelector(".btn-next").addEventListener("click", () => {
          nextSlide(true);
        });

        carousel.appendChild(ctrls);

        if (settings.slidenav || settings.animate) {
          slidenav = document.createElement("ul");

          slidenav.className = "slidenav";

          if (settings.animate) {
            const li = document.createElement("li");

            const animated = settings.startAnimated
              ? {
                  action: "stop",
                  visual: "Stop Animation",
                  icon: '<i class="fa fa-stop" aria-hidden="true"></i>',
                }
              : {
                  action: "start",
                  visual: "Start Animation",
                  icon: '<i class="fa fa-play" aria-hidden="true"></i>',
                };

            li.innerHTML = `<button data-action="${animated.action}"><span class="visuallyhidden">${animated.visual} </span>${animated.icon}</button>`;

            slidenav.appendChild(li);
          }

          if (settings.slidenav) {
            forEachElement(slides, (el, i) => {
              const li = document.createElement("li");
              const klass = i === 0 ? 'class="current" ' : "";
              const kurrent =
                i === 0
                  ? ' <span class="visuallyhidden">(Current Item)</span>'
                  : "";

              li.innerHTML = `<button ${klass}data-slide="${i}"><span class="visuallyhidden">Slide</span> ${
                i + 1
              }${kurrent}</button>`;
              slidenav.appendChild(li);
            });
          }

          let currentSlideNum;

          slidenav.addEventListener(
            "click",
            (event) => {
              const button = event.target;

              forEachElement(slides, (el, i) => {
                if (slides[i].classList.contains("current")) {
                  currentSlideNum = i;
                }
              });

              if (button.localName === "button") {
                if (button.getAttribute("data-slide")) {
                  stopAnimation();
                  if (button.getAttribute("data-slide") > currentSlideNum) {
                    if (
                      button.getAttribute("data-slide") == slides.length - 1 &&
                      currentSlideNum == 0
                    ) {
                      prevSlide(true);
                    } else {
                      setSlides(
                        button.getAttribute("data-slide"),
                        true,
                        "prev"
                      );
                    }
                  } else if (
                    button.getAttribute("data-slide") < currentSlideNum
                  ) {
                    if (
                      button.getAttribute("data-slide") == 0 &&
                      currentSlideNum == slides.length - 1
                    ) {
                      nextSlide(true);
                    } else {
                      setSlides(
                        button.getAttribute("data-slide"),
                        true,
                        "next"
                      );
                    }
                  }
                } else if (button.getAttribute("data-action") === "stop") {
                  stopAnimation();
                } else if (button.getAttribute("data-action") === "start") {
                  startAnimation();
                }
              }
            },
            true
          );

          carousel.className = "active carousel with-slidenav";
          carousel.appendChild(slidenav);
        }

        const liveregion = document.createElement("div");
        liveregion.setAttribute("aria-live", "polite");
        liveregion.setAttribute("aria-atomic", "true");
        liveregion.setAttribute("class", "liveregion visuallyhidden");
        carousel.appendChild(liveregion);

        slides[0].parentNode.addEventListener("transitionend", (event) => {
          const slide = event.target;
          removeClass(slide, "in-transition");
          if (hasClass(slide, "current")) {
            if (setFocus) {
              slide.setAttribute("tabindex", "-1");
              // slide.focus(); TCC-22
              setFocus = false;
            }
          }
        });

        carousel.addEventListener("focusin", (event) => {
          if (!hasClass(event.target, "slide")) {
            suspendAnimation();
          }
        });

        carousel.addEventListener("focusout", (event) => {
          if (!hasClass(event.target, "slide") && animationSuspended) {
            startAnimation();
          }
        });

        index = 0;
        setSlides(index);

        if (settings.startAnimated) {
          timer = setTimeout(nextSlide, setMotionDuration);
        }
      };

      return {
        init,
        next: nextSlide,
        prev: prevSlide,
        goto: setSlides,
        stop: stopAnimation,
        start: startAnimation,
      };
    };

    const c = new MyCarousel();
    c.init({
      id: "carouselOne",
      slidenav: true,
      animate: true,
      startAnimated: false,
    });
  };

  /*
  const containerMargin = () => {
    let navHeight = $("." + prefix + "-navigation").height();

    $("#content.main-wrapper").css({ "margin-top": navHeight + 20 });
  };
  */

  const footerPaddingWithCookieNotice = () => {
    let cookieNoticeH = $("#cookie-notice").height();
    $("body").css({ "padding-bottom": cookieNoticeH });
  };

  $("#cn-accept-cookie").on("click", () => {
    $("body").css({ "padding-bottom": 0 });
  });

  navigation();

  if ($("." + prefix + "-slider li").length > 1) {
    slider();
  }

  /* containerMargin(); */
  footerPaddingWithCookieNotice();

  $(window).on("resize", () => {
    /* containerMargin(); */
    if ($("#cookie-notice").length > 0) {
      footerPaddingWithCookieNotice();
    }
  });

  $(".skip-content").on("click", (e) => {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $("#content").offset().top,
      },
      500
    );
  });

  $(".skip-sidebar").on("click", (e) => {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $("#sidebar").offset().top,
      },
      500
    );
  });

  $(".skip-footer").on("click", (e) => {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $("#footer").offset().top,
      },
      500
    );
  });

  $(".skip-search").on("click", (e) => {
    e.preventDefault();
    if (window.matchMedia("(min-width: 992px)").matches) {
        $(`.${prefix}-navbar .search`).focus();
    }else{
        const $navMob = $(`.${prefix}-navbar-mobile`);
        $navMob.slideDown(250).promise().done(() => {
            $navMob.find(".search").focus();
        });
    }
  });
});

(function($){
	"use strict";

	const prefix = "cla";

	$(window).on("load", () => {
		const containerMargin = () => {
			let navHeight = $("." + prefix + "-navigation").height();

			$("#content.main-wrapper").css({ "margin-top": navHeight + 20 });
		};
		containerMargin();
		$(window).on("resize", () => {
			containerMargin();
		});
	});
})(jQuery);
