<?php

// Enable Featured Image on posts
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-thumbnails', array( 'post' ) );          // Posts only

// Theme Image Sizes
$themeSizes = array(
	array(
		'name'   => 'carousel',
		'width'  => 1140,
		'height' => 641,
	),
	array(
		'name'   => 'blog-image',
		'width'  => 730,
		'height' => 411,
	),
	array(
		'name'   => 'featured-image',
		'width'  => 510,
		'height' => 287,
	),
);

// Removes Static Front Page option from customizer
function themename_customize_register( $wp_customize ) {
	$wp_customize->remove_section( 'static_front_page' );
}

add_action( 'customize_register', 'themename_customize_register' );

// Add custom classes to wp_nav() list elements
function add_classes_on_li( $classes, $item, $args ) {
	$classes[] = 'custom_nav';

	return $classes;
}

add_filter( 'nav_menu_css_class', 'add_classes_on_li', 1, 3 );

// Register Custom Menus
function register_my_menus() {
	register_nav_menus(
		array(
			'top-nav'	=> __( 'Top Navigation' ),
			'page-menu'	=> __( 'Page Menu' ),
			'blog-menu'	=> __( 'Blog Post Menu' ),
		)
	);
}

add_action( 'init', 'register_my_menus' );

function col_custom_homepage_sidebar() {
	register_sidebar(
		array(
			'name'          => 'Home Page Sidebar',
			'id'            => 'homepage-side-bar',
			'description'   => 'Add widget that you want to show on Homepage',
			'before_widget' => '<div class="widget-content">',
			'after_widget'  => "</div>",
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'col_custom_homepage_sidebar' );

// custom image and post size definitions
@ini_set( 'upload_max_size', '64M' );
@ini_set( 'post_max_size', '64M' );
@ini_set( 'max_execution_time', '300' );

add_theme_support( 'yoast-seo-breadcrumbs' );

function register_svg_file_type( $mime_types ) {
	$mime_types['svg'] = 'image/svg+xml'; //Adding svg extension

	return $mime_types;
}

add_filter( 'upload_mimes', 'register_svg_file_type', 1, 1 );

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

require_once get_template_directory() . '/class-wp-bootstrap-walkerpage.php';

// Enqueue stylesheet and JS assets
require_once get_template_directory() . '/lib/enqueue-assets.php';

// Pagination for Archive and Index pages
require_once get_template_directory() . '/lib/pagination.php';

// Theme Options Page
require_once get_template_directory() . '/lib/theme-options.php';
require_once get_template_directory() . '/lib/theme-options-fields.php';

// Header Options Page
require_once get_template_directory() . '/lib/header-settings.php';
require_once get_template_directory() . '/lib/header-settings-fields.php';

// Footer Options Page
require_once get_template_directory() . '/lib/footer-settings.php';
require_once get_template_directory() . '/lib/footer-settings-fields.php';

// Home Page Options Page
require_once get_template_directory() . '/lib/home-page-settings.php';
require_once get_template_directory() . '/lib/home-page-settings-fields.php';

// Staff Page Options Page
require_once get_template_directory() . '/lib/staff-page-settings.php';
require_once get_template_directory() . '/lib/staff-page-settings-fields.php';

// custom dimensions to match the actual display size on desktop
foreach ( $themeSizes as $image ) {
	add_image_size( $image['name'], $image['width'], $image['height'] );
}

// Check if the Advanced Custom Fields plugin is active
if ( function_exists( 'get_field' ) ) {

	// Add color scheme class to body
	function inter_body_classes( $classes ) {

		if ( get_field( 'col_theme_choice', 'theme_options' ) == 'default' ) {
			return $classes;
		}

		$classes[] = get_field( 'col_theme_choice', 'theme_options' );

		return $classes;

	add_filter( 'body_class', 'inter_body_classes' );

	}

} else {
	// Do nothing
}

/* *
 * Customize Sidebar Menu for Bootstrap list groups https://getbootstrap.com/docs/4.3/components/list-group/
 * -- Joseph Dickson
 */

class Walker_Sidebar_Menu extends Walker_Nav_Menu {

	/**
	 *https://codex.wordpress.org/Class_Reference/Walker
	 * At the start of each element, output a <li> and <a> tag structure.
	 *
	 * Note: Menu objects include url and title properties, so we will use those.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$output .= sprintf( "\n<a class='list-group-item list-group-item-action' href='%s'%s>%s</a>\n",
			$item->url,
			( $item->object_id === get_the_ID() ) ? ' class="current"' : '',
			$item->title
		);
	}

}

function inter_get_image_dimensions_by_src( $src ) {

	global $wpdb, $themeSizes;

	$return = array();

	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $src ) );

	if ( $attachment[0] ) {

		foreach ( $themeSizes as $sizes ) {
			$return[ $sizes['name'] ] = wp_get_attachment_image_src( $attachment[0], $sizes['name'] )[0];
		}

		$return['logo-retina']   = wp_get_attachment_image_src( $attachment[0], 'logo-retina' )[0];
		$return['logo-retinaX2'] = wp_get_attachment_image_src( $attachment[0], 'logo-retinaX2' )[0];

	}

	return $return;

}

function inter_create_wide_screen_image( $imageData ) {

	global $themeSizes;

	$upload_dir = wp_upload_dir();

	foreach ( $themeSizes as $imageSize ) {

		$name = $imageSize['name'];

		if ( isset( $imageData['sizes'][ $name ] ) ) {
			$staticWidth  = $imageSize['width'];
			$staticHeight = $imageSize['height'];

			$srcImage = $upload_dir['path'] . '/' . $imageData['sizes'][ $name ]['file'];

			$renamedImage = preg_replace( '/-[0-9]{1,}x[0-9]{1,}/', '-' . $staticWidth . 'x' . $staticHeight, $imageData['sizes'][ $name ]['file'] );

			$newTempImage = "$srcImage.tmp";

			$newTempImage = inter_scale_crop( $srcImage, $newTempImage, $staticWidth, $staticHeight, 'middle' );

			unlink( $srcImage );

			$new_filename = preg_replace( '/-[0-9]{1,}x[0-9]{1,}/', '-' . $staticWidth . 'x' . $staticHeight, $srcImage );

			rename( $newTempImage, $new_filename );

			$imageData['sizes'][ $name ]['width']  = $staticWidth;
			$imageData['sizes'][ $name ]['height'] = $staticHeight;
			$imageData['sizes'][ $name ]['file']   = $renamedImage;
		}

	}

	return $imageData;
}

add_filter( 'wp_generate_attachment_metadata', 'inter_create_wide_screen_image' );

function inter_scale_crop( $src, $dest, $destW, $destH, $anchor ) {
	if ( ! file_exists( $dest ) && is_file( $src ) && is_readable( $src ) ) {
		$srcSize   = getimagesize( $src );
		$srcW      = $srcSize[0];
		$srcH      = $srcSize[1];
		$srcT      = $srcSize[2];
		$srcRatio  = $srcW / $srcH;
		$destRatio = $destW / $destH;

		switch ( $srcT ) {
			case 1:
				$img = ( imagecreatefromgif( $src ) );
				break;
			case 2:
				$img = ( imagecreatefromjpeg( $src ) );
				break;
			case 3:
				$img = ( imagecreatefrompng( $src ) );
				break;
		}

		$imgNew = imagecreatetruecolor( $destW, $destH );

		if ( $srcRatio < $destRatio ) {
			$scale = $srcW / $destW;
		} elseif ( $srcRatio >= $destRatio ) {
			$scale = $srcH / $destH;
		}
		$srcX = ( $srcW - ( $destW * $scale ) ) / 2;
		if ( $anchor = 'middle' ) {
			$srcY = ( $srcH - ( $destH * $scale ) ) / 2;
		} elseif ( $anchor = 'top' ) {
			$srcY = 0;
		} elseif ( $anchor = 'bottom' ) {
			$srcY = $srcH - ( $destH * $scale );
		}
		if ( $srcX < 0 ) {
			$srcX = 0;
		};
		if ( $srcY < 0 ) {
			$srcY = 0;
		};

		imagecopyresampled( $imgNew, $img, 0, 0, $srcX, $srcY, $destW, $destH, $destW * $scale, $destH * $scale );

		switch ( $srcT ) {
			case 1:
				imagegif( $imgNew, $dest );
				break;
			case 2:
				imagejpeg( $imgNew, $dest, 70 );
				break;
			case 3:
				imagepng( $imgNew, $dest );
				break;
		}

		imagedestroy( $img );
		imagedestroy( $imgNew );
	}

	return $dest;
}

// Retina Logo Size

add_image_size( 'logo-retina', 9999, 90, false );

function inter_image_make_retina_size( $file, $width, $height, $crop = false ) {
	$filename = '';
	if ( $height == 90 ) {
		if ( $width || $height ) {
			$editor = wp_get_image_editor( $file );
			if ( ! is_wp_error( $editor ) ) {
				$newWidth  = $width * 2;
				$newHeight = $height * 2;
				$editor->resize( $newWidth, $newHeight, $crop );
				$filename = $editor->generate_filename( $width . 'x' . $height . '@2x' );
				$editor->save( $filename );
			}
			if ( ! is_wp_error( $editor ) && $editor && $info = getimagesize( $filename ) ) {
				$filename = apply_filters( 'inter_retina_image_make_intermediate_size', $filename );

				return array(
					'file'      => wp_basename( $filename ),
					'width'     => $info[0],
					'height'    => $info[1],
					'mime-type' => $info['mime']
				);
			}
		}
	}

	return false;
}

function inter_generate_retina_image_metadata( $metadata, $attachment_id ) {
	$file         = get_attached_file( $attachment_id );
	$old_metadata = $metadata;
	foreach ( $metadata as $k => $v ) {
		if ( is_array( $v ) ) {
			foreach ( $v as $key => $val ) {
				if ( is_array( $val ) ) {
					inter_image_make_retina_size( $file, $val['width'], $val['height'], true );
					if ( $key == 'logo-retina' ) {
						$old_metadata[ $k ]['logo-retinaX2'] = inter_image_make_retina_size( $file, $val['width'], $val['height'], true );
					}
				}
			}
		}
	}

	return $old_metadata;
}

add_filter( 'wp_generate_attachment_metadata', 'inter_generate_retina_image_metadata', 10, 2 );

function inter_delete_retina_images( $attachment_id ) {
	$inter_metas     = wp_get_attachment_metadata( $attachment_id );
	$inter_updir     = wp_upload_dir();
	$inter_path      = pathinfo( $inter_metas['file'] );
	$inter_path_name = $inter_path['dirname'];
	$inter_updir     = wp_upload_dir();
	foreach ( $inter_metas as $inter_meta => $inter_meta_val ) {
		if ( $inter_meta === "sizes" ) {
			foreach ( $inter_meta_val as $inter_sizes => $inter_size ) {
				$inter_original_filename = $inter_updir['basedir'] . "/" . $inter_path_name . "/" . $inter_size['file'];
				$inter_x2_filename       = substr_replace( $inter_original_filename, "@2x.", strrpos( $inter_original_filename, "." ), strlen( "." ) );
				if ( file_exists( $inter_x2_filename ) ) {
					unlink( $inter_x2_filename );
				}
			}
		}
	}
}

add_filter( 'delete_attachment', 'inter_delete_retina_images' );

function image_tag_class( $class ) {
	$class .= ' img-fluid';

	return $class;
}

add_filter( 'get_image_tag_class', 'image_tag_class' );


//add_filter( 'wp_nav_menu_objects', 'inter_wp_nav_menu_objects_sub_menu', 10, 2 );

function inter_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
	if ( isset( $args->sub_menu ) ) {
		$root_id = 0;

		// find the current menu item
		foreach ( $sorted_menu_items as $menu_item ) {
			if ( $menu_item->current ) {
				// set the root id based on whether the current menu item has a parent or not
				$root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
				break;
			}
		}

		// find the top level parent
		if ( ! isset( $args->direct_parent ) ) {
			$prev_root_id = $root_id;
			while ( $prev_root_id != 0 ) {
				foreach ( $sorted_menu_items as $menu_item ) {
					if ( $menu_item->ID == $prev_root_id ) {
						$prev_root_id = $menu_item->menu_item_parent;
						// don't set the root_id to 0 if we've reached the top of the menu
						if ( $prev_root_id != 0 ) {
							$root_id = $menu_item->menu_item_parent;
						}
						break;
					}
				}
			}
		}

		$menu_item_parents = array();
		foreach ( $sorted_menu_items as $key => $item ) {
			// init menu_item_parents
			if ( $item->ID == $root_id ) {
				$menu_item_parents[] = $item->ID;
			}
			if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
				// part of sub-tree: keep!
				$menu_item_parents[] = $item->ID;
			} else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
				// not part of sub-tree: away with it!
				unset( $sorted_menu_items[ $key ] );
			}
		}

		return $sorted_menu_items;
	} else {
		return $sorted_menu_items;
	}
}

function inter_has_submenu( $post_id ) {

	$child_pages = false;

	if ( $post_id > 0 ) {

		$locations        = get_nav_menu_locations();
		$menu             = get_term( $locations['primary'], 'nav_menu' );
		$menu_items       = wp_get_nav_menu_items( $menu->term_id );
		$subArray         = array();
		$subSubArray      = array();
		$parentArray      = array();
		$currentMenuArray = array();
		$parentArrayLast  = array();
		$subArrayLast     = array();
		foreach ( $menu_items as $menu ) {
			if ( $menu->object_id == $post_id ) {
				$currentMenuArray = array(
					'db_id'            => $menu->db_id,
					'menu_item_parent' => $menu->menu_item_parent
				);
			} elseif ( $menu->menu_item_parent == 0 ) {
				$parentArray[] = array(
					'db_id'            => $menu->db_id,
					'menu_item_parent' => $menu->menu_item_parent
				);
			} else {
				$subArray[] = array(
					'db_id'            => $menu->db_id,
					'menu_item_parent' => $menu->menu_item_parent
				);
			}
		}

		$parentArrayLast = array_filter( $parentArray, function ( $menu ) use ( $currentMenuArray ) {
			return ( $menu['db_id'] == $currentMenuArray['menu_item_parent'] );
		} );

		$subArrayLast = array_filter( $subArray, function ( $menu ) use ( $currentMenuArray ) {
			return ( $menu['menu_item_parent'] == $currentMenuArray['db_id'] );
		} );

		$subSubArray = array_filter( $subArray, function ( $menu ) use ( $currentMenuArray, $parentArray ) {
			if ( $menu['db_id'] == $currentMenuArray['menu_item_parent'] ) {
				return in_array( $menu['menu_item_parent'], array_column( $parentArray, 'db_id' ) );
			}
		} );


		if ( ! empty( $parentArrayLast ) || ! empty( $subArrayLast ) || ! empty( $subSubArray ) ) {
			$child_pages = true;
		}

	}

	return $child_pages;
}

function has_children( $post_id ) {

	$children = get_pages( array( 'child_of' => $post_id ) );
	if ( count( $children ) == 0 ) {
		return false;
	} else {
		return true;
	}
}

// Displays Main Menu and Sidebar Menus
function wpb_list_child_pages() {

	global $post;

	$post_id = $post->ID;

	$current_page = get_queried_object_id();

	$temporary = get_post_ancestors( $post_id );

	$post_id = end( $temporary );

	if ( function_exists( 'get_field' ) ) {

		// Check if Theme Menu has Excluded Pages
		$exclude_pages = get_field( 'col_main_and_side_menu_exclude_option', 'theme_options' );

		if ( $exclude_pages ) {

			// Convert any pages to a string
			$exclude_pages = implode( $exclude_pages );

		} 
		
		// Display Parent Page menu excluding pages if any else display child and exclude pages if any
		if ( is_page() && $post->post_parent ) {
		
			$child_pages = wp_list_pages( 'depth=5&sort_column=menu_order&title_li=&child_of=' . $post_id . '&echo=0&exclude=' . $exclude_pages  );

		} else {

			$child_pages = wp_list_pages( 'depth=5&sort_column=menu_order&title_li=&child_of=' . $post_id . '&echo=0&exclude=' . $exclude_pages );

		}

	if ( $child_pages ) {

		$output = '
        <nav class="page-nav" id="sidebar-menu">
           	<ul>
              	<li class="page_item page-item-' . $post_id . '' . ( ( $current_page == $post_id ) ? ' current_page_item' : '' ) . '">
              	<a href="' . get_permalink( $post_id ) . '">' . get_the_title( $post_id ) . '</a>
              	<ul> 
              	' . $child_pages . '
	    		</ul>
	    		</li>
           	</ul>
	</nav>';

	}

		return $output;

	} else {

		// Display Parent Page menu excluding pages if any else display child and exclude pages if any
		if ( is_page() && $post->post_parent ) {
		
			$child_pages = wp_list_pages( 'depth=5&sort_column=menu_order&title_li=&child_of=' . $post_id . '&echo=0' );

		} else {

			$child_pages = wp_list_pages( 'depth=5&sort_column=menu_order&title_li=&child_of=' . $post_id . '&echo=0' );

		}

	if ( $child_pages ) {

		$output = '
        <nav class="page-nav" id="sidebar-menu">
           	<ul>
              	<li class="page_item page-item-' . $post_id . '' . ( ( $current_page == $post_id ) ? ' current_page_item' : '' ) . '">
              	<a href="' . get_permalink( $post_id ) . '">' . get_the_title( $post_id ) . '</a>
              	<ul> 
              	' . $child_pages . '
	    		</ul>
	    		</li>
           	</ul>
	</nav>';

	}

		return $output;

	}

}

if ( ! function_exists ( 'remove_class_filter' ) ) :
function remove_class_filter( $tag, $class_name = '', $method_name = '', $priority = 10 ) {
	global $wp_filter;

	// Check that filter actually exists first
	if ( ! isset( $wp_filter[ $tag ] ) ) {
		return false;
	}

	/**
	 * If filter config is an object, means we're using WordPress 4.7+ and the config is no longer
	 * a simple array, rather it is an object that implements the ArrayAccess interface.
	 *
	 * To be backwards compatible, we set $callbacks equal to the correct array as a reference (so $wp_filter is updated)
	 *
	 * @see https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/
	 */
	if ( is_object( $wp_filter[ $tag ] ) && isset( $wp_filter[ $tag ]->callbacks ) ) {
		// Create $fob object from filter tag, to use below
		$fob       = $wp_filter[ $tag ];
		$callbacks = &$wp_filter[ $tag ]->callbacks;
	} else {
		$callbacks = &$wp_filter[ $tag ];
	}

	// Exit if there aren't any callbacks for specified priority
	if ( ! isset( $callbacks[ $priority ] ) || empty( $callbacks[ $priority ] ) ) {
		return false;
	}

	// Loop through each filter for the specified priority, looking for our class & method
	foreach ( (array) $callbacks[ $priority ] as $filter_id => $filter ) {

		// Filter should always be an array - array( $this, 'method' ), if not goto next
		if ( ! isset( $filter['function'] ) || ! is_array( $filter['function'] ) ) {
			continue;
		}

		// If first value in array is not an object, it can't be a class
		if ( ! is_object( $filter['function'][0] ) ) {
			continue;
		}

		// Method doesn't match the one we're looking for, goto next
		if ( $filter['function'][1] !== $method_name ) {
			continue;
		}

		// Method matched, now let's check the Class
		if ( get_class( $filter['function'][0] ) === $class_name ) {

			// WordPress 4.7+ use core remove_filter() since we found the class object
			if ( isset( $fob ) ) {
				// Handles removing filter, reseting callback priority keys mid-iteration, etc.
				$fob->remove_filter( $tag, $filter['function'], $priority );

			} else {
				// Use legacy removal process (pre 4.7)
				unset( $callbacks[ $priority ][ $filter_id ] );
				// and if it was the only filter in that priority, unset that priority
				if ( empty( $callbacks[ $priority ] ) ) {
					unset( $callbacks[ $priority ] );
				}
				// and if the only filter for that tag, set the tag to an empty array
				if ( empty( $callbacks ) ) {
					$callbacks = array();
				}
				// Remove this filter from merged_filters, which specifies if filters have been sorted
				unset( $GLOBALS['merged_filters'][ $tag ] );
			}

			return true;
		}
	}

	return false;
}
endif;

function cod_remove_filter() {
	remove_class_filter( 'mce_external_plugins', 'Formstack_Plugin', 'mce_external_plugins' );
}

add_action( 'admin_init', 'cod_remove_filter' );

add_filter( 'theme_page_templates', 'cod_remove_page_template' );

function cod_remove_page_template( $pages_templates ) {
	$currntUserRole = get_current_user_role();

	if ( $currntUserRole[0] == 'editor' ) {
		unset( $pages_templates['../public/views/revslider-page-template.php'] );
		unset( $pages_templates['blog.php'] );
		unset( $pages_templates['search.php'] );
	}

	return $pages_templates;
}

function get_current_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles        = $current_user->roles;

	return $roles ? $roles : null; // returns roles if any found, else returns null
}

if ( is_admin() ) {

	function remove_revolution_slider_meta_boxes() {
		remove_meta_box( 'mymetabox_revslider_0', 'page', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'post', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'tribe_events', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'tribe_organizer', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'tribe_venue', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'accordions', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'slick_slider', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'acf-field-group', 'normal' );
	}

	add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );

}

add_action( 'admin_enqueue_scripts', 'cod_admin_theme_style' );
add_action( 'login_enqueue_scripts', 'cod_admin_theme_style' );

function cod_admin_theme_style() {
	if ( cod_current_user_has_role( 'editor' ) ) {
		echo '<style>.update-nag, .updated, .error, .is-dismissible { display: none; }</style>';
	}
	?>
    <style type="text/css">

        @media (min-width: 782px) {
            .post-type-tribe_events .wp-list-table th#title,
            .post-type-tribe_events .wp-list-table th#recurring {
                width: 25%;
            }
        }

        @media (min-width: 782px) and (max-width: 1200px) {
            .post-type-tribe_events .wp-list-table .column-events-cats,
            .post-type-tribe_events .wp-list-table .column-tags,
            .post-type-tribe_events .wp-list-table .column-author {
                display: none !important;
            }
        }
    </style>
	<?php
}

if ( ! function_exists( 'current_user_has_role' ) ) {
	function cod_current_user_has_role( $role ) {

		$user = get_userdata( get_current_user_id() );
		if ( ! $user || ! $user->roles ) {
			return false;
		}

		if ( is_array( $role ) ) {
			return array_intersect( $role, (array) $user->roles ) ? true : false;
		}

		return in_array( $role, (array) $user->roles );
	}
}

function cod_remove_menus() {
	if ( cod_current_user_has_role( 'editor' ) ) {
		remove_menu_page( 'edit-comments.php' );          //Comments
	}
}

add_action( 'admin_menu', 'cod_remove_menus' );

add_filter('mce_buttons', 'cod_remove_button_from_tmce', 99);

function cod_remove_button_from_tmce($button){

    $key = array_search('revslider_sc_button', $button);
	if ( false !== $key ) {
		unset( $button[ $key ] );
	}

    return $button;
}

function code_add_unfiltered_html_capability_to_editors( $caps, $cap, $user_id ) {
	if ( ('unfiltered_html' === $cap && user_can( $user_id, 'administrator' )) || ('unfiltered_html' === $cap &&user_can( $user_id, 'editor' )) ) {
		$caps = array( 'unfiltered_html' );
	}
	return $caps;
}
add_filter( 'map_meta_cap', 'code_add_unfiltered_html_capability_to_editors', 1, 3 );
