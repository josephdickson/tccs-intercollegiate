<?php
/**
 * Enqueue Assets - enqueue theme required stylesheets and javascript
 * Author: Jospeh Dickson joseph_dickson@pitzer.edu
 *
 * @package WordPress
 * @subpackage TCCS Intercollegiate
 * @since TCCS Intercollegiate 2.0*
 */

namespace Intercollegiate;

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_assets' );

function enqueue_assets() {

	wp_enqueue_style(
		'fontawesome-fonts',
		get_stylesheet_directory_uri() . '/webfonts/fonts.css',
		array(),
		true
	);

	wp_enqueue_style(
		'fontawesome-style',
		get_stylesheet_directory_uri() . '/webfonts/all.min.css',
		array(),
		true
	);

	wp_enqueue_script(
		'fontawesome-script',
		get_stylesheet_directory_uri() . '/webfonts/all.min.js',
		array(),
	);


	wp_enqueue_style(
		'intercollegiate-style',
		get_stylesheet_uri(),
		array(),
		true
	);


	// Check that Advanced Custom Fields Plugin is Active and assign default theme CSS
	if ( function_exists( 'get_field' ) ) {

		$themeScheme = (!empty(get_field( 'col_theme_choice', 'theme_options' ))) ? 'style_' . get_field( 'col_theme_choice', 'theme_options' ) . '.css': 'style_theme_1.css';
	
		wp_enqueue_style(
			'bootstrap-style',
			get_template_directory_uri() . '/public/stylesheets/' . $themeScheme,
			array(),	
			true
		);

	} else {
		// if ACF is not active assign default theme CSS
		wp_enqueue_style(
			'bootstrap-style',
			get_template_directory_uri() . '/public/stylesheets/style_theme_1.css',
			array(),	
			true
		);

	}

	wp_enqueue_script(
		'fitvids-js',
		get_stylesheet_directory_uri() . '/lib/jquery.fitvids.js',
		array( 'jquery' )
	);

	wp_enqueue_script(
		'tccs-scripts-js',
		get_stylesheet_directory_uri() . '/lib/tccs-scripts.js',
		array( 'fitvids-js' )
	);
}

/**
 * Enqueue theme CSS in Editor only.
 */
add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\tccs_enqueue_block_editor_assets' );

function tccs_enqueue_block_editor_assets() {

	wp_enqueue_style(
		'block-style',
		get_stylesheet_directory_uri() . '/css/blocks.css',
		null,
		time() //change to filemtime for production to allow caching
	);

}
