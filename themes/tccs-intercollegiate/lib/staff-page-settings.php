<?php

function code_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {

	if( $text = get_sub_field('cod_group_title', 'staff_options') ) {

		$title = $text;

	}

	return $title;

}

// name
add_filter('acf/fields/flexible_content/layout_title/name=cod_group_options', 'code_acf_flexible_content_layout_title', 10, 4);

