<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d65ab49c0582',
	'title' => 'Theme Options',
	'fields' => array(
		array(
			'key' => 'field_5d65ac914ac0f',
			'label' => 'Theme Choice',
			'name' => 'col_theme_choice',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'theme_1' => 'Blue Theme',
				'theme_2' => 'Gold Theme',
				'theme_4' => 'Green Theme',
				'theme_5' => 'Purple Theme',
				'theme_3' => 'Red Theme',
				'theme_6' => 'White Theme',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'theme_1',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_5d7177cec2603',
			'label' => 'Main and Side Menu Exclude Option',
			'name' => 'col_main_and_side_menu_exclude_option',
			'type' => 'post_object',
			'instructions' => 'Please select page or pages which you want to hide in Main and Sidebar Menu',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'page',
			),
			'taxonomy' => '',
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'id',
			'ui' => 1,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'inter_theme_options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;
