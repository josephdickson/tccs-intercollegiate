<?php
/*
 *
 * Template Name: Google Search
 *
*/

get_header();

?>

<div class="container">
    <!-- Header image -->
	<?php
	get_template_part( 'template-parts/common/ribbon', 'common' );
	?>
</div>

<div class="container">
    <div class="row">

        <div class="col-lg-12">

			<?php
			get_template_part( 'template-parts/common/breadcrumbs', 'common' ); // Page Navigation
			?>

            <h1 class="text-center">
				<?php the_title(); ?>
            </h1>

            <div class="main">
				<?php
				if ( have_rows( 'col_google_settings', 'header_options' ) ) {
					while ( have_rows( 'col_google_settings', 'header_options' ) ): the_row();
						$googleCustomSearchId = get_sub_field( 'col_google_custom_search_id', 'header_options' );

						if ( ! empty( $googleCustomSearchId ) ) {
							?>
                            <script async
                                    src="https://cse.google.com/cse.js?cx=<?php echo $googleCustomSearchId; ?>"></script>
                            <div class="gcse-search"></div>
							<?php
						}
					endwhile;
				}
				?>
            </div>

        </div>

    </div>
</div>

<?php get_footer(); ?>
