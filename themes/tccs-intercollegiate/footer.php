<?php
/**
 * Displays the Footer
 *
 * Requires Advanced Custom Fields Plugin for some features
 * ACF settings can be set in the Admin Dashboard's Footer menu
 *
 */

// Check if the Advanced Custom Fields plugin is installed and activated
if ( function_exists( 'get_field' ) ) {

	$wdget_area_visibilty = get_field( 'col_footer_widget_area_visiblity', 'footer_options' );
	$footer_logo_display = get_field('col_footer_logos_display_option', 'footer_options');
	$footer_tccs_display = get_field('col_footer_logos_hide_show_tccs', 'footer_options');

	if ( $wdget_area_visibilty ) {
		$column = get_field( 'col_footer_column', 'footer_options' );

		if ( have_rows( 'col_column_content', 'footer_options' ) ) {
			while ( have_rows( 'col_column_content', 'footer_options' ) ) {
				the_row();
				$first_column  = get_sub_field( 'col_first_column_content', 'footer_options' );
				$second_column = get_sub_field( 'col_second_column_content', 'footer_options' );
				$third_column  = get_sub_field( 'col_third_column_content', 'footer_options' );
			}
		}
    
		echo '<section class="footer-widget-area">';
		echo '<div class="container-fluid">';
		echo '<div class="row">';
		echo '<div class="container">';
		echo ( $column == 'one_column' ) ? '<div class="row justify-content-center text-center">'  : '<div class="row text-center text-md-left">';


		if ( $column == 'one_column' ) {

			echo '<div class="col-12">';
			echo $first_column; 
			echo '</div>';

		} elseif ( $column == 'two_column' ) {

			echo '<div class="col-12 col-md-3">';
			echo $first_column;
			echo '</div>';
			echo '<div class="col-12 col-md-9">';
			echo $second_column;
			echo'</div>';

		} elseif ( $column == 'three_column' ) {
			echo '<div class="col-12 col-md-4">';
			echo $first_column;
			echo '</div>';
			echo '<div class="col-12 col-md-4">';
			echo $second_column;
			echo '</div>';
			echo '<div class="col-12 col-md-4">';
			echo $third_column;
			echo '</div>';
		}

		echo '</div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		echo '</section>';
	}
}

?>
<footer id="footer">
    <div class="container footer">
        <div class="row">
            <div class="span12">
                <div class="footer-logos display-<?php echo $footer_logo_display; ?>">
<?php

// Check if the Advanced Custom Fields plugin is installed and activated
if ( function_exists( 'get_field' ) ) {

/**
 * Footer Logo Settings
 * Requires Advanced Custom Fields plugin
 * 5C, 7C and TCCS logos
 */
	// Pomona College 
	if($footer_logo_display == "5C" || $footer_logo_display == "7C") {

		echo '<a href="https://www.pomona.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-pom"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Pomona College website</span>';
		echo '</a>';

	}

	// Claremont Graduate University
	if($footer_logo_display == "7C") {

		echo '<a href="https://www.cgu.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-cgu"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Claremont Graduate University website</span>';
		echo '</a>';

	}

	// TCCS
	if($footer_logo_display == "7C" && $footer_tccs_display) {

		echo '<a href="https://services.claremont.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-cuc"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to The Claremont Colleges Services\' website</span>';
		echo '</a>';

	}

	// Scripps College
	if($footer_logo_display == "5C" || $footer_logo_display == "7C") {

		echo '<a href="http://www.scrippscollege.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-scripps"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Scripps College website</span>';
		echo '</a>';

	}

	// Claremont McKenna College
	if($footer_logo_display == "5C" || $footer_logo_display == "7C") {
		
		echo '<a href="http://www.cmc.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-cmc"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Claremont McKenna College website</span>';
		echo '</a>';
	}

	// Harvey Mudd College
	if($footer_logo_display == "5C" || $footer_logo_display == "7C") {

		echo '<a href="https://www.hmc.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-hmc"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Harvey Mudd College website</span>';
		echo '</a>';
	}

	// Pitzer College
	if($footer_logo_display == "5C" || $footer_logo_display == "7C") {
		echo '<a href="https://www.pitzer.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-pitz"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Pitzer College website</span>';
		echo '</a>';
	}

	// Keck Graduate Institute
	if($footer_logo_display == "7C") {
		echo '<a href="http://www.kgi.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-kgi"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Keck Graduate Institute website</span>';
		echo '</a>';
	}

} else {
	/**
	 *  Display all 7C &  TCCS logos if Advanced Custom Fields plugin is not active
	 *
	 *  Pomona College 
	 */
		echo '<a href="https://www.pomona.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-pom"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Pomona College website</span>';
		echo '</a>';

	// Claremont Graduate University

		echo '<a href="https://www.cgu.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-cgu"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Claremont Graduate University website</span>';
		echo '</a>';

	// TCCS

		echo '<a href="https://services.claremont.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-cuc"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to The Claremont Colleges Services\' website</span>';
		echo '</a>';

	// Scripps College

		echo '<a href="http://www.scrippscollege.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-scripps"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Scripps College website</span>';
		echo '</a>';

	// Claremont McKenna College
		
		echo '<a href="http://www.cmc.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-cmc"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Claremont McKenna College website</span>';
		echo '</a>';

	// Harvey Mudd College

		echo '<a href="https://www.hmc.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-hmc"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Harvey Mudd College website</span>';
		echo '</a>';

	// Pitzer College
	
		echo '<a href="https://www.pitzer.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-pitz"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Pitzer College website</span>';
		echo '</a>';

	// Keck Graduate Institute 

		echo '<a href="http://www.kgi.edu/" target="_blank">';
		echo '<svg class="svg-icon">';
		echo '<use xlink:href="#icon-logo-kgi"></use>';
		echo '</svg>';
		echo '<span class="sr-only">Go to the Keck Graduate Institute website</span>';
		echo '</a>';

}

?>
                </div>
            </div>
        </div>
    </div>
</section>
</footer>

</div> <!-- end of Main Wrapper -->

<?php wp_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/javascript/main.js"></script>

</body>
</html>
