<?php get_header(); ?>

<div class="container">
	<?php
	get_template_part( 'template-parts/common/breadcrumbs', 'common' );
	?>

	<div class="row">

		<div class="col-sm-12 col-md-8">

			<?php get_template_part('template-parts/common/excerpt', 'common'); ?>

			<?php get_template_part('template-parts/common/pagination'); ?>

		</div>

		<div class="col-sm-12 col-md-4">

			<div id="sidebar" class="list-group">

				<?php get_template_part('template-parts/common/posts', 'common'); // Post Navigation ?>

                <div class="mt-2"></div>

				<?php get_template_part('template-parts/common/category', 'common'); // Category Filter ?>

                <div class="mt-2"></div>

			</div>

		</div>

	</div><!-- .row -->

</div><!-- .container .archive -->

<?php get_footer(); ?>
