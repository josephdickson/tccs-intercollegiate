# VAGRANT

```sh
$ git clone -b master git://github.com/Varying-Vagrant-Vagrants/VVV.git ~/Sites/vagrant-local
$ cd ~/Sites/vagrant-local
```

# ADDING SITE

Once you’ve installed everything, copy vvv-config.yml to vvv-custom.yml. Any changes to vvv-config.yml will get overwritten when you update VVV, so always make changes to vvv-custom.yml:

Add the following under Sites:

```sh
  tccs-intercollegiate-theme-4:
    repo: https://github.com/Varying-Vagrant-Vagrants/custom-site-template.git
    nginx_upstream: php56
    hosts:
      - tccs-intercollegiate-theme-4.test
    custom:
      wp_version: 4.9.8
```

Still in vvv-custom.yml, check the utilities section to be sure php56 is listened there:

```
utilities:
  core:
    - memcached-admin
    - opcache-status
    - phpmyadmin
    - webgrind
    - trusted-hosts
    - tls-ca
    - php56
    - php71
```

# CONFIGURE GUEST MACHINE

```sh
$ vagrant up --provision
$ cd ~/vagrant-local/www/tccs-intercollegiate-theme-4/
```

Verify that default install is accessible via localhost by going to

http://vvv.test/ and
http://tccs-intercollegiate-theme-4.test/

# CLONE REPOSITORY

If default wordpress install is showing then proceed with cloning repo into install's themes

```sh
$ cd wp-content/themes/
$ git clone git@bitbucket.org:gocodistry/tccs-intercollegiate-theme-4.git 
```

# ACTIVATE CUSTOM THEME FROM WORDPRESS DASHBOARD

On the browser go to http://tccs-intercollegiate-theme-4.test/wp-admin/
Login with the following credentials:
username: admin
password: password

Go to Dashboard -> Appearance --> Themes
http://tccs-intercollegiate-theme-4.test/wp-admin/themes.php

and activate "Claremont Colleges" Theme

# THEME CONFIGURATION
Settings:
- Reading
- Permalinks:
Set to Post Name

Plugins
- Advanced Custom fields
Create the following group and fields on admin dashboard:
Field Group:
Featured

Field
Label: Home page featured test 
Name: home_page_featured_test_
Input type: Text Area

- Contact Form 7
- Contact Form 7 Accessible Defaults
- The Events Calendar
- Yoast SEO

Set rules to:
Post category is equal to 'featured'

Pages
- Home
- Search
- Blog
Posts

Menus
Create the following Menus (Dashboard -> Appearance -> Menus)
Top Navigation
Sidebar Navigation
Quicklinks Navigation


# DATABASE

# BROWSERSYNC

```sh
$ npm install -g browser-sync
$ browser-sync --version
```

Commands
```sh
$ browser-sync start [options]     Start Browsersync
$ browser-sync init                Create a configuration file
$ browser-sync reload              Send a reload event over HTTP protocol
$ browser-sync recipe [name]       Generate the files for a recipe
$ browser-sync start --help
```

Initiating Browsersynch in project. Execute the following commands

```sh
browser-sync start --proxy "tccs-intercollegiate-theme-4.test" --files "**/*"
```

