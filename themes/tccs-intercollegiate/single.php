<?php get_header(); ?>

<div class="container post">

	<?php
	get_template_part( 'template-parts/common/title', 'common' );

	?>

    <div class="row">
        <div class="col-sm-12 col-md-8">

			<?php
			// Display .post-meta-box
			get_template_part( 'template-parts/common/post', 'meta' );

			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					the_content();
				}
			}
			?>

        </div>

        <div class="col-sm-12 col-md-4">

			<div id="sidebar" class="list-group">

				<?php get_template_part('template-parts/common/posts', 'common'); // Post Navigation ?>

                <div class="mt-2"></div>

				<?php get_template_part('template-parts/common/category', 'common'); // Category Filter ?>

                <div class="mt-2"></div>

			</div>
        </div>

    </div>
</div>

<?php get_footer(); ?>
