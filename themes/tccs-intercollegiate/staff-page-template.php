<?php
/*
 *
 * Template Name: Staff Page Template
 *
*/
get_header();

global $post;

$childpages = '';

if ( $post->ID > 0 ):

	if ( $post->post_parent ):
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
	else:
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
	endif;

endif;

$classLeft = 'col-12 text-center text-md-left';

if ( $childpages ) {
	$classLeft = 'col-lg-8 text-center text-md-left';
}
?>

<div class="container staff">
	<?php
	get_template_part( 'template-parts/common/title', 'common' );
	?>
    <div class="row">

        <div class="<?php echo $classLeft; ?>">
			<?php if ( have_rows( 'cod_group_options', $post->ID ) ): ?>
				<?php
				while ( have_rows( 'cod_group_options', $post->ID ) ): the_row();
					$groupTitle = get_sub_field( 'cod_group_title', $post->ID );
					?>
                    <div class="row member-group">
                        <h4 class="mx-auto ml-md-0"><?php echo $groupTitle; ?></h4>
                        <div class="col-12 p-0">
							<?php
							if ( have_rows( 'cod_group_member', $post->ID ) ):
								while ( have_rows( 'cod_group_member', $post->ID ) ): the_row();
									$memberImageID = get_sub_field( 'cod_group_member_photo', $post->ID );
									$memberName    = get_sub_field( 'cod_group_member_name', $post->ID );
									$memberTitle   = get_sub_field( 'cod_group_member_title', $post->ID );
									$memberEmail   = get_sub_field( 'cod_group_member_email', $post->ID );
									$memberPhone   = get_sub_field( 'cod_group_member_phone', $post->ID );
									$memberDetails = get_sub_field( 'cod_group_member_details', $post->ID );
									?>
                                    <div class="card">
                                        <div class="row">
                                            <div class="col-4 col-md-3 mx-auto">
												<?php if ( $memberImageID ) {
													echo wp_get_attachment_image( $memberImageID, 'full', "", array( "class" => "img-fluid card-img border border-dark" ) );
												} ?>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="card-body text-center text-md-left">
												<?php if ( ! empty( $memberName ) ): ?><h6
                                                        class="font-weight-bold"><?php echo $memberName; ?></h6><?php endif; ?>
													<?php if ( ! empty( $memberTitle ) ): ?><p class="card-text">
                                                        <em><?php echo $memberTitle; ?></em></p><?php endif; ?>
													<?php if ( ! empty( $memberEmail ) ): ?><p class="card-text"><span
                                                            class="font-weight-bold"><?php _e( 'Email:', 'tccs-intercollegiate' ); ?></span> <a href="mailto:<?php echo $memberEmail; ?>"><?php echo $memberEmail; ?></a>
                                                        </p><?php endif; ?>
													<?php if ( ! empty( $memberPhone ) ): ?><p
                                                            class="card-text"><?php echo $memberPhone; ?></p><?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
												<?php if ( ! empty( $memberDetails ) ): ?><p
                                                        class="card-text"><?php echo $memberDetails; ?></p><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
								<?php
								endwhile;
							endif;
							?>
                        </div>
                    </div>
				<?php
				endwhile;
			else:
				_e( 'No staff was found in site', 'tccs-intercollegiate' );
			endif;
			?>
        </div>

		<?php if ( $childpages ) : ?>
            <div class="col-lg-4 d-none d-lg-flex">
				<?php
				echo wpb_list_child_pages();
				?>
            </div>
		<?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
