function _typeof2(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

/**
 * breakpoints-js v1.0.6
 * https://github.com/amazingSurge/breakpoints-js
 *
 * Copyright (c) amazingSurge
 * Released under the LGPL-3.0 license
 */
(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports'], factory);
  } else if (typeof exports !== 'undefined') {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.breakpointsEs = mod.exports;
  }
})(this, function (exports) {
  'use strict';

  Object.defineProperty(exports, '__esModule', {
    value: true
  });

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (_typeof2(call) === 'object' || typeof call === 'function') ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== 'function' && superClass !== null) {
      throw new TypeError('Super expression must either be null or a function, not ' + _typeof2(superClass));
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError('Cannot call a class as a function');
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ('value' in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var _typeof = typeof Symbol === 'function' && _typeof2(Symbol.iterator) === 'symbol' ? function (obj) {
    return _typeof2(obj);
  } : function (obj) {
    return obj && typeof Symbol === 'function' && obj.constructor === Symbol && obj !== Symbol.prototype ? 'symbol' : _typeof2(obj);
  };
  /**
   * breakpoints-js v1.0.6
   * https://github.com/amazingSurge/breakpoints-js
   *
   * Copyright (c) amazingSurge
   * Released under the LGPL-3.0 license
   */


  var defaults = {
    // Extra small devices (phones)
    xs: {
      min: 0,
      max: 767
    },
    // Small devices (tablets)
    sm: {
      min: 768,
      max: 991
    },
    // Medium devices (desktops)
    md: {
      min: 992,
      max: 1199
    },
    // Large devices (large desktops)
    lg: {
      min: 1200,
      max: Infinity
    }
  };
  var util = {
    each: function each(obj, fn) {
      var continues = void 0;

      for (var i in obj) {
        if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object' || obj.hasOwnProperty(i)) {
          continues = fn(i, obj[i]);

          if (continues === false) {
            break; //allow early exit
          }
        }
      }
    },
    isFunction: function isFunction(obj) {
      return typeof obj === 'function' || false;
    },
    extend: function extend(obj, source) {
      for (var property in source) {
        obj[property] = source[property];
      }

      return obj;
    }
  };

  var Callbacks = function () {
    function Callbacks() {
      _classCallCheck(this, Callbacks);

      this.length = 0;
      this.list = [];
    }

    _createClass(Callbacks, [{
      key: 'add',
      value: function add(fn, data) {
        var one = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        this.list.push({
          fn: fn,
          data: data,
          one: one
        });
        this.length++;
      }
    }, {
      key: 'remove',
      value: function remove(fn) {
        for (var i = 0; i < this.list.length; i++) {
          if (this.list[i].fn === fn) {
            this.list.splice(i, 1);
            this.length--;
            i--;
          }
        }
      }
    }, {
      key: 'empty',
      value: function empty() {
        this.list = [];
        this.length = 0;
      }
    }, {
      key: 'call',
      value: function call(caller, i) {
        var fn = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

        if (!i) {
          i = this.length - 1;
        }

        var callback = this.list[i];

        if (util.isFunction(fn)) {
          fn.call(this, caller, callback, i);
        } else if (util.isFunction(callback.fn)) {
          callback.fn.call(caller || window, callback.data);
        }

        if (callback.one) {
          delete this.list[i];
          this.length--;
        }
      }
    }, {
      key: 'fire',
      value: function fire(caller) {
        var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        for (var i in this.list) {
          if (this.list.hasOwnProperty(i)) {
            this.call(caller, i, fn);
          }
        }
      }
    }]);

    return Callbacks;
  }();

  var ChangeEvent = {
    current: null,
    callbacks: new Callbacks(),
    trigger: function trigger(size) {
      var previous = this.current;
      this.current = size;
      this.callbacks.fire(size, function (caller, callback) {
        if (util.isFunction(callback.fn)) {
          callback.fn.call({
            current: size,
            previous: previous
          }, callback.data);
        }
      });
    },
    one: function one(data, fn) {
      return this.on(data, fn, true);
    },
    on: function on(data, fn) {
      var one = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      if (typeof fn === 'undefined' && util.isFunction(data)) {
        fn = data;
        data = undefined;
      }

      if (util.isFunction(fn)) {
        this.callbacks.add(fn, data, one);
      }
    },
    off: function off(fn) {
      if (typeof fn === 'undefined') {
        this.callbacks.empty();
      }
    }
  };

  var MediaQuery = function () {
    function MediaQuery(name, media) {
      _classCallCheck(this, MediaQuery);

      this.name = name;
      this.media = media;
      this.initialize();
    }

    _createClass(MediaQuery, [{
      key: 'initialize',
      value: function initialize() {
        this.callbacks = {
          enter: new Callbacks(),
          leave: new Callbacks()
        };
        this.mql = window.matchMedia && window.matchMedia(this.media) || {
          matches: false,
          media: this.media,
          addListener: function addListener() {// do nothing
          },
          removeListener: function removeListener() {// do nothing
          }
        };
        var that = this;

        this.mqlListener = function (mql) {
          var type = mql.matches && 'enter' || 'leave';
          that.callbacks[type].fire(that);
        };

        this.mql.addListener(this.mqlListener);
      }
    }, {
      key: 'on',
      value: function on(types, data, fn) {
        var one = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

        if ((typeof types === 'undefined' ? 'undefined' : _typeof(types)) === 'object') {
          for (var type in types) {
            if (types.hasOwnProperty(type)) {
              this.on(type, data, types[type], one);
            }
          }

          return this;
        }

        if (typeof fn === 'undefined' && util.isFunction(data)) {
          fn = data;
          data = undefined;
        }

        if (!util.isFunction(fn)) {
          return this;
        }

        if (typeof this.callbacks[types] !== 'undefined') {
          this.callbacks[types].add(fn, data, one);

          if (types === 'enter' && this.isMatched()) {
            this.callbacks[types].call(this);
          }
        }

        return this;
      }
    }, {
      key: 'one',
      value: function one(types, data, fn) {
        return this.on(types, data, fn, true);
      }
    }, {
      key: 'off',
      value: function off(types, fn) {
        var type = void 0;

        if ((typeof types === 'undefined' ? 'undefined' : _typeof(types)) === 'object') {
          for (type in types) {
            if (types.hasOwnProperty(type)) {
              this.off(type, types[type]);
            }
          }

          return this;
        }

        if (typeof types === 'undefined') {
          this.callbacks.enter.empty();
          this.callbacks.leave.empty();
        } else if (types in this.callbacks) {
          if (fn) {
            this.callbacks[types].remove(fn);
          } else {
            this.callbacks[types].empty();
          }
        }

        return this;
      }
    }, {
      key: 'isMatched',
      value: function isMatched() {
        return this.mql.matches;
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this.off();
      }
    }]);

    return MediaQuery;
  }();

  var MediaBuilder = {
    min: function min(_min) {
      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'px';
      return '(min-width: ' + _min + unit + ')';
    },
    max: function max(_max) {
      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'px';
      return '(max-width: ' + _max + unit + ')';
    },
    between: function between(min, max) {
      var unit = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'px';
      return '(min-width: ' + min + unit + ') and (max-width: ' + max + unit + ')';
    },
    get: function get(min, max) {
      var unit = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'px';

      if (min === 0) {
        return this.max(max, unit);
      }

      if (max === Infinity) {
        return this.min(min, unit);
      }

      return this.between(min, max, unit);
    }
  };

  var Size = function (_MediaQuery) {
    _inherits(Size, _MediaQuery);

    function Size(name) {
      var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Infinity;
      var unit = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'px';

      _classCallCheck(this, Size);

      var media = MediaBuilder.get(min, max, unit);

      var _this = _possibleConstructorReturn(this, (Size.__proto__ || Object.getPrototypeOf(Size)).call(this, name, media));

      _this.min = min;
      _this.max = max;
      _this.unit = unit;
      var that = _this;

      _this.changeListener = function () {
        if (that.isMatched()) {
          ChangeEvent.trigger(that);
        }
      };

      if (_this.isMatched()) {
        ChangeEvent.current = _this;
      }

      _this.mql.addListener(_this.changeListener);

      return _this;
    }

    _createClass(Size, [{
      key: 'destroy',
      value: function destroy() {
        this.off();
        this.mql.removeListener(this.changeListener);
      }
    }]);

    return Size;
  }(MediaQuery);

  var UnionSize = function (_MediaQuery2) {
    _inherits(UnionSize, _MediaQuery2);

    function UnionSize(names) {
      _classCallCheck(this, UnionSize);

      var sizes = [];
      var media = [];
      util.each(names.split(' '), function (i, name) {
        var size = Breakpoints$1.get(name);

        if (size) {
          sizes.push(size);
          media.push(size.media);
        }
      });
      return _possibleConstructorReturn(this, (UnionSize.__proto__ || Object.getPrototypeOf(UnionSize)).call(this, names, media.join(',')));
    }

    return UnionSize;
  }(MediaQuery);

  var info = {
    version: '1.0.6'
  };
  var sizes = {};
  var unionSizes = {};

  var Breakpoints = window.Breakpoints = function () {
    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    Breakpoints.define.apply(Breakpoints, args);
  };

  Breakpoints.defaults = defaults;
  Breakpoints = util.extend(Breakpoints, {
    version: info.version,
    defined: false,
    define: function define(values) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (this.defined) {
        this.destroy();
      }

      if (!values) {
        values = Breakpoints.defaults;
      }

      this.options = util.extend(options, {
        unit: 'px'
      });

      for (var size in values) {
        if (values.hasOwnProperty(size)) {
          this.set(size, values[size].min, values[size].max, this.options.unit);
        }
      }

      this.defined = true;
    },
    destroy: function destroy() {
      util.each(sizes, function (name, size) {
        size.destroy();
      });
      sizes = {};
      ChangeEvent.current = null;
    },
    is: function is(size) {
      var breakpoint = this.get(size);

      if (!breakpoint) {
        return null;
      }

      return breakpoint.isMatched();
    },
    all: function all() {
      var names = [];
      util.each(sizes, function (name) {
        names.push(name);
      });
      return names;
    },
    set: function set(name) {
      var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Infinity;
      var unit = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'px';
      var size = this.get(name);

      if (size) {
        size.destroy();
      }

      sizes[name] = new Size(name, min, max, unit);
      return sizes[name];
    },
    get: function get(size) {
      if (sizes.hasOwnProperty(size)) {
        return sizes[size];
      }

      return null;
    },
    getUnion: function getUnion(sizes) {
      if (unionSizes.hasOwnProperty(sizes)) {
        return unionSizes[sizes];
      }

      unionSizes[sizes] = new UnionSize(sizes);
      return unionSizes[sizes];
    },
    getMin: function getMin(size) {
      var obj = this.get(size);

      if (obj) {
        return obj.min;
      }

      return null;
    },
    getMax: function getMax(size) {
      var obj = this.get(size);

      if (obj) {
        return obj.max;
      }

      return null;
    },
    current: function current() {
      return ChangeEvent.current;
    },
    getMedia: function getMedia(size) {
      var obj = this.get(size);

      if (obj) {
        return obj.media;
      }

      return null;
    },
    on: function on(sizes, types, data, fn) {
      var one = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
      sizes = sizes.trim();

      if (sizes === 'change') {
        fn = data;
        data = types;
        return ChangeEvent.on(data, fn, one);
      }

      if (sizes.includes(' ')) {
        var union = this.getUnion(sizes);

        if (union) {
          union.on(types, data, fn, one);
        }
      } else {
        var size = this.get(sizes);

        if (size) {
          size.on(types, data, fn, one);
        }
      }

      return this;
    },
    one: function one(sizes, types, data, fn) {
      return this.on(sizes, types, data, fn, true);
    },
    off: function off(sizes, types, fn) {
      sizes = sizes.trim();

      if (sizes === 'change') {
        return ChangeEvent.off(types);
      }

      if (sizes.includes(' ')) {
        var union = this.getUnion(sizes);

        if (union) {
          union.off(types, fn);
        }
      } else {
        var size = this.get(sizes);

        if (size) {
          size.off(types, fn);
        }
      }

      return this;
    }
  });
  var Breakpoints$1 = Breakpoints;
  exports["default"] = Breakpoints$1;
});

jQuery(document).ready(function ($) {
  var prefix = "cla";
  Breakpoints();

  var navigation = function navigation() {
    var $iconMainMenuWrapper = $(".".concat(prefix, "-header .").concat(prefix, "-hamburger-icon-wrapper"));
    var $navMob = $(".".concat(prefix, "-navbar-mobile"));
    var header = $(".".concat(prefix, "-header"));
    var top = 192;
    var didResize;
    $(window).resize(function () {
      didResize = true;
    });

    var smallDevicesSwitcher = function smallDevicesSwitcher() {
      if (Breakpoints.is("md")) {
        $iconMainMenuWrapper.find(".hamburger-icon").removeClass("animate");
        $navMob.slideUp(0);
      }
    };

    setInterval(function () {
      if (didResize) {
        smallDevicesSwitcher();
        didResize = false;
      }
    }, 250);
    $iconMainMenuWrapper.click(function () {
      $(this).find(".hamburger-icon").toggleClass("animate");
      $navMob.slideToggle(250);
    });
    $(window).scroll(function () {
      if ($(window).scrollTop() >= top) {
        header.addClass("minimize");
      } else {
        header.removeClass("minimize");
      }
    });
  };

  var w = window;
  var d = w.document;

  var addPolyfill = function addPolyfill(e) {
    var type = e.type === "focus" ? "focusin" : "focusout";
    var event = new CustomEvent(type, {
      bubbles: true,
      cancelable: false
    });
    event.c1Generated = true;
    e.target.dispatchEvent(event);
  };

  var removePolyfill = function removePolyfill(e) {
    if (!e.c1Generated) {
      // focus after focusin, so chrome will the first time trigger tow times focusin
      d.removeEventListener("focus", addPolyfill, true);
      d.removeEventListener("blur", addPolyfill, true);
      d.removeEventListener("focusin", removePolyfill, true);
      d.removeEventListener("focusout", removePolyfill, true);
    }

    setTimeout(function () {
      d.removeEventListener("focusin", removePolyfill, true);
      d.removeEventListener("focusout", removePolyfill, true);
    });
  };

  if (w.onfocusin === undefined) {
    d.addEventListener("focus", addPolyfill, true);
    d.addEventListener("blur", addPolyfill, true);
    d.addEventListener("focusin", removePolyfill, true);
    d.addEventListener("focusout", removePolyfill, true);
  }

  var slider = function slider() {
    var MyCarousel = function MyCarousel() {
      var setMotionDuration = 5000;
      var carousel;
      var slides;
      var index;
      var slidenav;
      var settings;
      var timer;
      var setFocus;
      var animationSuspended;

      var setSlides = function setSlides(newCurrent, setFocusHere, transition, announceItemHere) {
        setFocus = typeof setFocusHere !== "undefined" ? setFocusHere : false;
        var announceItem = typeof announceItemHere !== "undefined" ? announceItemHere : false;
        var trans = typeof transition !== "undefined" ? transition : "none";
        var newCurr = parseFloat(newCurrent);
        var _slides = slides,
            length = _slides.length;
        var newNext = newCurr + 1;
        var newPrev = newCurr - 1;

        if (newNext === length) {
          newNext = 0;
        } else if (newPrev < 0) {
          newPrev = length - 1;
        }

        for (var i = slides.length - 1; i >= 0; i -= 1) {
          slides[i].className = "slide";
        }

        slides[newNext].className = "next slide".concat(trans === "next" ? " in-transition" : "");

        if (length === 2) {
          if (newCurr === 0) {
            slides[newNext].className = "prevtwo next slide in-transition";
          } else if (newCurr === 1) {
            slides[newNext].className = "nexttwo next slide in-transition";
          }
        }

        slides[newNext].setAttribute("aria-hidden", "true");

        if (newNext !== newPrev) {
          slides[newPrev].className = "prev slide".concat(trans === "prev" ? " in-transition" : "");
          slides[newPrev].setAttribute("aria-hidden", "true");
        }

        slides[newCurr].className = "current slide";
        slides[newCurr].removeAttribute("aria-hidden");

        if (announceItem) {
          carousel.querySelector(".liveregion").textContent = "Item ".concat(newCurr + 1, " of ").concat(slides.length);
        }

        if (settings.slidenav) {
          var buttons = carousel.querySelectorAll(".slidenav button[data-slide]");

          for (var j = buttons.length - 1; j >= 0; j -= 1) {
            buttons[j].className = "";
            buttons[j].innerHTML = "<span class=\"visuallyhidden\">Slide</span> ".concat(j + 1);
          }

          buttons[newCurr].className = "current";
          buttons[newCurr].innerHTML = "<span class=\"visuallyhidden\">Slide</span> ".concat(newCurr + 1, " <span class=\"visuallyhidden\">(Current Item)</span>");
        }

        index = newCurr;
      };

      var nextSlide = function nextSlide(announceItem) {
        var annItem = typeof announceItem !== "undefined" ? announceItem : false;
        var _slides2 = slides,
            length = _slides2.length;
        var newCurrent = index + 1;

        if (newCurrent === length) {
          newCurrent = 0;
        }

        setSlides(newCurrent, false, "prev", annItem);

        if (settings.animate) {
          timer = setTimeout(nextSlide, setMotionDuration);
        }
      };

      var prevSlide = function prevSlide(announceItem) {
        var annItem = typeof announceItem !== "undefined" ? announceItem : false;
        var _slides3 = slides,
            length = _slides3.length;
        var newCurrent = index - 1;

        if (newCurrent < 0) {
          newCurrent = length - 1;
        }

        setSlides(newCurrent, false, "next", annItem);
      };

      var stopAnimation = function stopAnimation() {
        clearTimeout(timer);
        settings.animate = false;
        animationSuspended = false;
        var btn = carousel.querySelector("[data-action]");
        btn.innerHTML = '<span class="visuallyhidden">Start Animation </span><i class="fa fa-play" aria-hidden="true"></i>';
        btn.setAttribute("data-action", "start");
      };

      var startAnimation = function startAnimation() {
        settings.animate = true;
        animationSuspended = false;
        timer = setTimeout(nextSlide, setMotionDuration);
        var btn = carousel.querySelector("[data-action]");
        btn.innerHTML = '<span class="visuallyhidden">Stop Animation </span><i class="fa fa-stop" aria-hidden="true"></i>';
        btn.setAttribute("data-action", "stop");
        btn.classList.add("stop");
      };

      var suspendAnimation = function suspendAnimation() {
        if (settings.animate) {
          clearTimeout(timer);
          settings.animate = false;
          animationSuspended = true;
        }
      };

      var forEachElement = function forEachElement(elements, fn) {
        for (var i = 0; i < elements.length; i += 1) {
          fn(elements[i], i);
        }
      };

      var removeClass = function removeClass(el, className) {
        var elem = el;

        if (elem.classList) {
          elem.classList.remove(className);
        } else {
          elem.className = elem.className.replace(new RegExp("(^|\\b)".concat(className.split(" ").join("|"), "(\\b|$)"), "gi"), " ");
        }
      };

      var hasClass = function hasClass(el, className) {
        if (el.classList) {
          return el.classList.contains(className);
        }

        return new RegExp("(^| )".concat(className, "( |$)"), "gi").test(el.className);
      };

      var init = function init(set) {
        settings = set;
        carousel = document.getElementById(settings.id);
        slides = carousel.querySelectorAll(".slide");
        carousel.className = "active carousel";
        var ctrls = document.createElement("ul");
        ctrls.className = "controls full";
        ctrls.innerHTML = "\n        <li class=\"button-prev\">\n          <button type=\"button\" class=\"btn-prev\"><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i></button>\n        </li>\n        <li class=\"button-prev\">\n          <button type=\"button\" class=\"btn-next\"><i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></button>\n        </li>";
        ctrls.querySelector(".btn-prev").addEventListener("click", function () {
          prevSlide(true);
        });
        ctrls.querySelector(".btn-next").addEventListener("click", function () {
          nextSlide(true);
        });
        carousel.appendChild(ctrls);

        if (settings.slidenav || settings.animate) {
          slidenav = document.createElement("ul");
          slidenav.className = "slidenav";

          if (settings.animate) {
            var li = document.createElement("li");
            var animated = settings.startAnimated ? {
              action: "stop",
              visual: "Stop Animation",
              icon: '<i class="fa fa-stop" aria-hidden="true"></i>'
            } : {
              action: "start",
              visual: "Start Animation",
              icon: '<i class="fa fa-play" aria-hidden="true"></i>'
            };
            li.innerHTML = "<button data-action=\"".concat(animated.action, "\"><span class=\"visuallyhidden\">").concat(animated.visual, " </span>").concat(animated.icon, "</button>");
            slidenav.appendChild(li);
          }

          if (settings.slidenav) {
            forEachElement(slides, function (el, i) {
              var li = document.createElement("li");
              var klass = i === 0 ? 'class="current" ' : "";
              var kurrent = i === 0 ? ' <span class="visuallyhidden">(Current Item)</span>' : "";
              li.innerHTML = "<button ".concat(klass, "data-slide=\"").concat(i, "\"><span class=\"visuallyhidden\">Slide</span> ").concat(i + 1).concat(kurrent, "</button>");
              slidenav.appendChild(li);
            });
          }

          var currentSlideNum;
          slidenav.addEventListener("click", function (event) {
            var button = event.target;
            forEachElement(slides, function (el, i) {
              if (slides[i].classList.contains("current")) {
                currentSlideNum = i;
              }
            });

            if (button.localName === "button") {
              if (button.getAttribute("data-slide")) {
                stopAnimation();

                if (button.getAttribute("data-slide") > currentSlideNum) {
                  if (button.getAttribute("data-slide") == slides.length - 1 && currentSlideNum == 0) {
                    prevSlide(true);
                  } else {
                    setSlides(button.getAttribute("data-slide"), true, "prev");
                  }
                } else if (button.getAttribute("data-slide") < currentSlideNum) {
                  if (button.getAttribute("data-slide") == 0 && currentSlideNum == slides.length - 1) {
                    nextSlide(true);
                  } else {
                    setSlides(button.getAttribute("data-slide"), true, "next");
                  }
                }
              } else if (button.getAttribute("data-action") === "stop") {
                stopAnimation();
              } else if (button.getAttribute("data-action") === "start") {
                startAnimation();
              }
            }
          }, true);
          carousel.className = "active carousel with-slidenav";
          carousel.appendChild(slidenav);
        }

        var liveregion = document.createElement("div");
        liveregion.setAttribute("aria-live", "polite");
        liveregion.setAttribute("aria-atomic", "true");
        liveregion.setAttribute("class", "liveregion visuallyhidden");
        carousel.appendChild(liveregion);
        slides[0].parentNode.addEventListener("transitionend", function (event) {
          var slide = event.target;
          removeClass(slide, "in-transition");

          if (hasClass(slide, "current")) {
            if (setFocus) {
              slide.setAttribute("tabindex", "-1"); // slide.focus(); TCC-22

              setFocus = false;
            }
          }
        });
        carousel.addEventListener("focusin", function (event) {
          if (!hasClass(event.target, "slide")) {
            suspendAnimation();
          }
        });
        carousel.addEventListener("focusout", function (event) {
          if (!hasClass(event.target, "slide") && animationSuspended) {
            startAnimation();
          }
        });
        index = 0;
        setSlides(index);

        if (settings.startAnimated) {
          timer = setTimeout(nextSlide, setMotionDuration);
        }
      };

      return {
        init: init,
        next: nextSlide,
        prev: prevSlide,
        "goto": setSlides,
        stop: stopAnimation,
        start: startAnimation
      };
    };

    var c = new MyCarousel();
    c.init({
      id: "carouselOne",
      slidenav: true,
      animate: true,
      startAnimated: false
    });
  };
  /*
  const containerMargin = () => {
    let navHeight = $("." + prefix + "-navigation").height();
     $("#content.main-wrapper").css({ "margin-top": navHeight + 20 });
  };
  */


  var footerPaddingWithCookieNotice = function footerPaddingWithCookieNotice() {
    var cookieNoticeH = $("#cookie-notice").height();
    $("body").css({
      "padding-bottom": cookieNoticeH
    });
  };

  $("#cn-accept-cookie").on("click", function () {
    $("body").css({
      "padding-bottom": 0
    });
  });
  navigation();

  if ($("." + prefix + "-slider li").length > 1) {
    slider();
  }
  /* containerMargin(); */


  footerPaddingWithCookieNotice();
  $(window).on("resize", function () {
    /* containerMargin(); */
    if ($("#cookie-notice").length > 0) {
      footerPaddingWithCookieNotice();
    }
  });
  $(".skip-content").on("click", function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: $("#content").offset().top
    }, 500);
  });
  $(".skip-sidebar").on("click", function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: $("#sidebar").offset().top
    }, 500);
  });
  $(".skip-footer").on("click", function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: $("#footer").offset().top
    }, 500);
  });
  $(".skip-search").on("click", function (e) {
    e.preventDefault();

    if (window.matchMedia("(min-width: 992px)").matches) {
      $(".".concat(prefix, "-navbar .search")).focus();
    } else {
      var $navMob = $(".".concat(prefix, "-navbar-mobile"));
      $navMob.slideDown(250).promise().done(function () {
        $navMob.find(".search").focus();
      });
    }
  });
});

(function ($) {
  "use strict";

  var prefix = "cla";
  $(window).on("load", function () {
    var containerMargin = function containerMargin() {
      var navHeight = $("." + prefix + "-navigation").height();
      $("#content.main-wrapper").css({
        "margin-top": navHeight + 20
      });
    };

    containerMargin();
    $(window).on("resize", function () {
      containerMargin();
    });
  });
})(jQuery);